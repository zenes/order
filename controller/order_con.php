<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  Order
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * order de la base de datos.
 *
 * @category Controlador
 * @package  Order
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Order_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_order;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/order.php";
        $this->_conf = new Config();
        $this->_order = new Order();
    }

    /**
     * Esta funcion se encarga de controlar la creacion de un
     * carro
     *
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post)
    {
        $this->_session(["admin", "chain"]);
        if ($this->_order->create($post)) {
             die(header("location:../../".$_SESSION["rol"]."/orders?success=1"));
        } else {
            header("location:../../".$_SESSION["rol"]."/order_reg?error=1");
        }
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la order que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_order->read($id);
    }

    /**
     * Esta funcion trae la informacion de los últimos 5 carros por id de cadena
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readLatest()
    {
        $this->_session();
        return $this->_order->readLatest($_SESSION["chain"]);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la cadena a la que pertenece el carro
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readTruck()
    {
        $this->_session();
        return $this->_order->readTruck($_SESSION["truck"]);
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un order
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post)
    {
        $this->_session(["admin", "chain"]);
        if ($this->_order->update($post)) { 
            die(header("location:../../".$_SESSION["rol"]."/orders?success=2"));
        } else {
            header(
                "location:../../".$_SESSION["rol"]."/order_reg?error=1&update=1
              &id_order=".$post['id_order']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del order a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session(["admin", "chain"]);
        if ($this->_order->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
