<?php
/**
 * PHP Version 7.4.3
 *
 * Este archivo se usa como router para hacer uso del controlador
 * la compronacion de sesion debe ser llamada por el controlador dependiendo 
 * de la necesidad de esta comprobacion
 *
 * @category Controlador
 * @package  Router
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
$conf = new Config();
if (!(in_array($class, $conf->getRutas())  
    || in_array($function, $conf->getRutas()))
) {
    header("location:".$conf->getUri("controller/logout.php"));
}
//si el metodo es post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (strpos($_SERVER['HTTP_REFERER'], $conf->getUri()) === false) {
        die(header("location:".$conf->getUri("controller/logout.php")));
    }
    $class = ucwords(ucfirst($class), "_");
    $objeto = new $class;
    $post = array();
    /*
     * Probablemente este paso consume mucho tiempo de procesamiento
     * pero es necesario para evitar inyecciones sql
     */
    foreach ($_POST as $key => $var) {
        $post+= array($key => htmlentities($var));
    }
    //VVV ESTE ES EL TRUCO MAS GENIAL DEL MUNDO VVV
    return $objeto->{$function}($post, $_FILES);
}
?>
