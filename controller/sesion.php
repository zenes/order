<?php
/**
 * PHP Version 7.4.3
 *
 * Este archivo es el que se encarga de verificar la sesion
 * de renovar los cookies de sesion y de ver si las redirecciones
 * se estan haciendo dentro de la aplicación
 *
 * @category Controlador
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/../config/config.php";
$conf = new Config();
$requestURI = explode('/', $_SERVER['REQUEST_URI']);
$class = $requestURI[count($requestURI)-2];
$function = $requestURI[count($requestURI)-1];
$function = strpos($function, "?") ? substr($function, 0, strpos($function, "?")) : $function;

if ($function != "login") {
    ini_set('session.cookie_samesite', 'Strict');
    session_set_cookie_params($conf->getTiempoSesion(), '/'.$conf->getAppName()); 
    session_name($conf->getAppName());
    session_start();
    /**
    * En este codigo se determina si el usuario ha iniciado sesion,
    * y de ser cierto se determina si el usuario esta activo para
    * regenerar el cookie de sesion o para cerrarle la sesion
    */
    if (empty($_SESSION['id'])  
        || empty($_SESSION['rol'])
    ) {
        die(header("location:".$conf->getUri("controller/logout")));
    }
    session_regenerate_id();
    session_write_close();
}
/**
  * Este trait debera ser incorporado en todas las classs ya 
  * que contiene una function que debe usarse antes de ejecutar
  * cualquier function o acceso a la capa del modelo.
  *
  * @category Controlador
  * @package  Controller
  * @class    Sesion
  * @author   José camilo Jiménez <jmilo@protonmaill.con>
  * @license  MIT
  * @link     https://pbear.xyz
  */
trait VerificacionSesion
{
    /**
     * Esta function se utiliza para verificar que el usuario
     * haya iniciado sesion antes de ejecutar una function.
     *
     * PD: Dejar la function de configuración como $this->_conf debido
     * a que así es como esta definida en las classs del Controlador 
     * y como este trait se incluye en los controladores no hay que 
     * modificar nada mas, de lo contrario pueden ocurrir errores.
     *
     * @param array/string $rol se usa string cuando la function 
     *                          solo le pertenece a un solo rol, y 
     *                          array cuando es una function que se puede ejecutar
     *                          por muchos roles pero no por todos. 
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */
    private function _session($rol = false)
    {
        /**
         * Si el usuario si ha iniciado sesion, y la function envia un rol
         * diferente a admin o director que son los que tienen todos los 
         * permisos en la plataforma se verificara que rol tiene y si tiene
         * derecho de ejecutar la function, de lo contrario se le cerrara sesion
         */
        if (is_array($rol)) {
            foreach ($rol as $valor) {
                if ($valor == $_SESSION['rol']) {
                    return;
                }
            }
            die(header("location:".$this->_conf->getUri("controller/logout")));
        } else if ($rol && $rol != $_SESSION['rol']) {
            header("location:".$this->_conf->getUri("controller/logout"));
        }
    }
}
?>
