<?php
/**
 * PHP version 7.4.3
 *
 * Este archivo se usa para cerrar la sesion del usuario.
 *
 * @category Controller
 * @package  Logout
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../config/config.php";
$conf = new Config();
session_set_cookie_params(3600, '/'.$conf->getAppName()); 
session_name($conf->getAppName());
session_start();
session_unset();
session_destroy();
setcookie($conf->getAppName(), "/".$conf->getAppName(), time()-3600);
header("location:/".$conf->getAppName());
?>
