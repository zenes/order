<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  User
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * user de la base de datos.
 *
 * @category Controlador
 * @package  User
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class User_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_user;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/user.php";
        $this->_conf = new Config();
        $this->_user = new User();
    }

    /**
     * Esta funcion inicia la sesion
     * 
     * @param array $post es un array con todos los datos para iniciar sesion
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function login($data)
    {
        $usr = $this->_user->readEmail($data["email"]);
        if (!$usr) {
            header(
                "location:".
                $this->_conf->getUri("?error=1")
            );
        }
        if (password_verify($data["password"], $usr->password)) {
                $tiempoSesion = $this->_conf->getTiempoSesion();
                session_set_cookie_params(
                    $tiempoSesion, 
                    '/'.$this->_conf->getAppName()
                );
                session_name($this->_conf->getAppName());
                session_start();
            if ($usr->id_role == 2) {
                include_once __DIR__."/chain_con.php";
                $chain_con = new Chain_Con();
                $chain = $chain_con->readManager($usr->id);
                $_SESSION["chain"] = $chain->id;
                $_SESSION["chain_name"] = $chain->name;
            } else if ($usr->id_role == 3) {
                include_once __DIR__."/truck_con.php";
                $truck_con = new Truck_Con();
                $truck = $truck_con->read($usr->id_truck);
                $_SESSION["truck"] = $usr->id_truck;
                $_SESSION["truck_name"] = $truck->name;
            }
                $_SESSION['user'] = $usr->name;
                $_SESSION['id'] = $usr->id;
                $_SESSION['email'] = $usr->email;
                $_SESSION['rol'] = $usr->role;
                $_SESSION['id_rol'] = $usr->id_role;
                session_write_close();
                die(
                    header(
                        "location:".
                        $this->_conf->getUri($usr->role."/home")
                    )
                );
        }
    }
    /**
     * Esta funcion retorna la carga del sistema como un json
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array
     */
    public function load()
    {
        $this->_session();
        die(json_encode(sys_getloadavg()));
    }
    /**
     * Esta funcion retorna la memoria del sistema como un json
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array
     */
    public function mem()
    {
        $this->_session();
        $datos = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach ($datos as $line) {
            @list($key, $val) = explode(":", $line);
            @$meminfo[$key] = trim($val);
        }
        die(json_encode($meminfo));
    }
    /**
     * Esta funcion se encarga de controlar la creacion de una
     * nueva cadena de restaurantes
     * 
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post)
    {
        $this->_session();
        if ($this->_user->create($post)) {
             die(header("location:../../".$_SESSION["rol"]."/users?success=1"));
        } else {
            header("location:../../".$_SESSION["rol"]."/user_reg?error=1");
        }
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la user que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_user->read($id);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readChain()
    {
        $this->_session();
        return $this->_user->readChain($_SESSION["chain"]);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la user que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readManagers()
    {
        $this->_session();
        return $this->_user->readManagers();
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param array $data contiene el id del/a user que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readJson($data)
    {
        $this->_session();
        $res = $this->_user->read($data["id"]);
        unset($res->password);
        die(json_encode($res));
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la user que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readClients()
    {
        $this->_session();
        return $this->_user->readClients();
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un user
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post)
    {
        $this->_session();
        if ($this->_user->update($post)) { 
            die(header("location:../../".$_SESSION["rol"]."/users?success=2"));
        } else {
            header(
                "location:../../".$_SESSION["rol"]."/user_reg?error=1&update=1
              &id_user=".$post['id_user']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del user a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session();
        if ($this->_user->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
