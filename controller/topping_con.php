<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  Topping
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * topping de la base de datos.
 *
 * @category Controlador
 * @package  Topping
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Topping_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_topping;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/topping.php";
        $this->_conf = new Config();
        $this->_topping = new Topping();
    }

    /**
     * Esta funcion se encarga de controlar la creacion de una
     * nueva cadena de restaurantes
     * 
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post)
    {
        $this->_session("truck");
        if ($this->_topping->create($post)) {
             die(header("location:../../".$_SESSION["rol"]."/toppings?success=1"));
        }
        header("location:../../".$_SESSION["rol"]."/topping_reg?error=1");
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la topping que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_topping->read($id);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readTruck()
    {
        $this->_session();
        return $this->_topping->readTruck($_SESSION["truck"]);
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un topping
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post)
    {
        $this->_session("truck");
        if ($this->_topping->update($post)) { 
            die(header("location:../../admin/topping?success=2"));
        } else {
            header(
                "location:../../admin/topping_reg?error=1&update=1
              &id_topping=".$post['id_topping']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del topping a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session("truck");
        if ($this->_topping->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
