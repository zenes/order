<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  Role
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * role de la base de datos.
 *
 * @category Controlador
 * @package  Role
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Role_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_role;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/role.php";
        $this->_conf = new Config();
        $this->_role = new Role();
    }

    /**
     * Esta funcion se encarga de controlar la creacion de una
     * nueva cadena de restaurantes
     * 
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post)
    {
        $this->_session("admin");
        if ($this->_role->create($post)) {
             die(header("location:../../admin/roles?success=1"));
        } else {
            header("location:../../admin/role_reg?error=1");
        }
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la role que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_role->read($id);
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un role
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post)
    {
        $this->_session("admin");
        if ($this->_role->update($post)) { 
            die(header("location:../../admin/roles?success=2"));
        } else {
            header(
                "location:../../admin/role_reg?error=1&update=1
              &id_role=".$post['id_role']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del role a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session("admin");
        if ($this->_role->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
