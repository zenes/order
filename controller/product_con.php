<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  Product
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * product de la base de datos.
 *
 * @category Controlador
 * @package  Product
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Product_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_product;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/product.php";
        $this->_conf = new Config();
        $this->_product = new Product();
    }

    /**
     * Esta funcion se encarga de controlar la creacion de un
     * carro
     *
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post, $files)
    {
        $this->_session(["admin", "truck"]);
        if ($id = $this->_product->create($post)) {
            $file = __DIR__."/../assets/uploads/products/".$id.".jpeg";
            if (file_exists($file)) { 
                unlink($file);
            }
            move_uploaded_file(
                $files["photo"]["tmp_name"],
                $file
            );
            $this->_product->updatePhoto(array("photo"=> $id.".jpeg", "id" => $id));
             die(header("location:../../".$_SESSION["rol"]."/products?success=1"));
        } else {
            header("location:../../".$_SESSION["rol"]."/product_reg?error=1");
        }
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la product que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_product->read($id);
    }

    /**
     * Esta funcion trae la informacion de los últimos 5 carros por id de cadena
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readLatest()
    {
        $this->_session();
        return $this->_product->readLatest($_SESSION["truck"]);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la cadena a la que pertenece el carro
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readTruck()
    {
        $this->_session();
        return $this->_product->readTruck($_SESSION["truck"]);
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un product
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post)
    {
        $this->_session(["admin", "truck"]);
        if ($this->_product->update($post)) { 
            die(header("location:../../".$_SESSION["rol"]."/products?success=2"));
        } else {
            header(
                "location:../../".$_SESSION["rol"]."/product_reg?error=1&update=1
              &id_product=".$post['id_product']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del product a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session(["admin", "truck"]);
        if ($this->_product->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
