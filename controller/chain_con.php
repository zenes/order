<?php
/**
 * PHP Version 7.4.3
 *
 * @category Controlador
 * @package  Chain
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/sesion.php";
/**
 * Esta clase es la encargada de representar el objeto 
 * chain de la base de datos.
 *
 * @category Controlador
 * @package  Chain
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Chain_Con
{
    use VerificacionSesion;
    private $_conf;
    private $_chain;

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        include_once __DIR__."/../model/chain.php";
        $this->_conf = new Config();
        $this->_chain = new Chain();
    }

    /**
     * Esta funcion se encarga de controlar la creacion de una
     * nueva cadena de restaurantes
     * 
     * @param array $post contiene la informacion del arreglo POST
     *                    enviado desde el front end.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return location
     */ 
    public function create($post, $files)
    {
        $this->_session("admin");
        if ($id = $this->_chain->create($post)) {
            move_uploaded_file(
                $files["photo"]["tmp_name"], 
                __DIR__."/../assets/uploads/chains/".$id.".jpeg"
            );
            $this->_chain->updatePhoto(array("photo"=> $id.".jpeg", "id" => $id));
            die(header("location:../../admin/chains?success=1"));
        } else {
            header("location:../../admin/chain_reg?error=1");
        }
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @param int $id es el id de la chain que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function read($id = false)
    {
        $this->_session();
        return $this->_chain->read($id);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos
     * 
     * @param int $id es el id de la chain que se quiere ver
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readManager($id)
    {
        $this->_session();
        return $this->_chain->readManager($id);
    }

    /**
     * Esta funcion trae la información desde los modelos y retorna un arreglo con los datos.
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function readLatest()
    {
        $this->_session();
        return $this->_chain->readLatest();
    }

    /**
     * Esta funcion se encarga de controlar la actualizacion de un chain
     * 
     * @param array $post es un arreglo con el id del _pais a editar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function update($post, $files)
    {
        $this->_session("admin");
        if ($this->_chain->update($post)) { 
            $file = __DIR__."/../assets/uploads/chains/".$post["id"].".jpeg";
            if (file_exists($file)) { 
                unlink($file);
            }
            move_uploaded_file(
                $files["photo"]["tmp_name"],
                $file
            );
            die(header("location:../../admin/chains?success=2"));
        } else {
            header(
                "location:../../admin/chain_reg?error=1&update=1
              &id_chain=".$post['id_chain']
            );
        }
    }

    /**
     * Esta funcion se encarga de controlar la eliminacion de una cadena de restaurantes
     * 
     * @param array $post es un arreglo con el id del chain a eliminar.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string (1 o 0) JSON
     */ 
    public function delete($post)
    {
        $this->_session("admin");
        if ($this->_chain->delete($post['id'])) {
            die("1");
        }
        die("0");
    }


}
/**
 * Luego de crear la clase en memoria, se llama al router que es el que luego se encarga
 * de llamar a sus metodos
 */
require_once __DIR__."/router.php";
?>
