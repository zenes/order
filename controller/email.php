<?php
/**
 * PHP version 7.4.3
 *
 * @category Controlador
 * @package  Email
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
// require_once __DIR__."/sesion.php";
require_once __DIR__."/../libs/mailer/PHPMailerAutoload.php";
require_once __DIR__."/../config/config.php";
require_once __DIR__."/../model/conf.php";
/**
 * PHP version 7.4.3
 *
 * @category Controlador
 * @package  Email
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class Email
{
    private $_conf;
    private $_mail;
    private $_smtp;
    private $_mailPass;
    private $_mailUser;
    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
        $this->_conf = new Config();   
        $key = $this->_conf->getKey(); // Llave de desencriptacion
        $this->_mail = new PHPMailer();
        $this->_smtp = $this->_conf->getEmail();

        $this->_mailPass = new Conf();
        $this->_mailPass = $this->_mailPass->email();
        $this->_mailUser = $this->_mailPass->email;
        $this->_mailPass = openssl_decrypt(
            $this->_mailPass->email_secret,
            $key['metodo'], $key['llave'], 0, $key['vector']
        ); //contraseña desencriptada
    }
    /**
     * Este metodo genera el header de los email
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    private function _header()
    {
        $this->_mail->SMTPDebug = false;                             // Enable verbose debug output
        $this->_mail->isSMTP();                                      // Set mailer to use SMTP
        $this->_mail->Host = $this->_smtp['server'];               // Specify main and backup SMTP servers
        $this->_mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->_mail->Username = $this->_mailUser;        // SMTP username
        $this->_mail->Password = $this->_mailPass;   // SMTP password
        $this->_mail->SMTPSecure = $this->_smtp['crypt'];      // Enable TLS encryption, `ssl` also accepted
        $this->_mail->Port = $this->_smtp['port'];                 // TCP port to connect to
    }
    /**
     * Este metodo genera el footer de los email y envía el correo
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    private function _footer()
    {
        if (!$this->_mail->send()) {
            echo 'Mailer Error: ' . $this->_mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }
    /**
     * Este metodo genera el footer de los email y envía el correo
     *
     * @param $correo  string es un string con el correo electronico al que
     *                 se desea envíar el mensaje.
     * @param $message string es el mensaje que se desea envíar
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function mensaje($correo, $message)
    {
        $this->_header();
        $this->_mail->From = $this->_mailUser;
        $this->_mail->FromName = $this->_smtp['username'];
        $this->_mail->addAddress($correo, 'Usuario');                // Add a recipient
        $this->_mail->isHTML(true);                                  // Set email format to HTML

        $this->_mail->CharSet = "UTF-8";
        $asunto = "Alertas ".$this->_conf->getAppName();
        $asunto = "=?UTF-8?B?" . base64_encode($asunto) . "=?=";
        $this->_mail->Subject = utf8_encode($asunto);
        $this->_mail->Body = "<!DOCTYPE html>
                         <html>
                            <head>
                               <meta charset=\"utf-8\">
                               <style>
                                  body
                                  {
                                     font-family: Arial;
                                     font-size: 13px;
                                     padding: 20px 10px;
                                     width: 780px;
                                     line-height: 20px;
                                     background: #0099cc;
                                  }
                               </style>
                            </head>
                            <body>
                               <div style=\"background:#32adc3;margin:0px; float:left; padding:5px 20px; color:#fff; width:820px; text-shadow:0px 0px 1px #000000; line-height:19px\">
<h4 style=\"float:right\">Alertas automáticas - No responder</h4>
<img align='left' src='".$this->_conf->getUri("assets/images/logo-light-text.png")."' width='200px' ><a href='".$this->_conf->getUri()."'></a></img>                                  
                               </div>
                               <div style=\"background:#FFFFFF;margin:0px; float:left; padding:5px 20px; color:#333333; width:820px\">
                                  <div>
                                     <table border=0>
                                        <tr>
                                           <td style=\"padding:10px\">
                                              ".$message."
                                           </td>
                                        </tr>
                                     </table>
                                  </div>
                               </div>
                               <div style=\"line-height:16px;background:#32adc3;margin:0 0 20px 0; float:left; padding:5px 20px; color:#fff; width:820px\">
                                  <span style=\"float:right\">Copyright &copy; ".Date('Y')."</span>
                               </div>
                            </body>
                         </html>";
        return $this->_footer();
    }
}
?>
