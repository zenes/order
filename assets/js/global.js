function verTerminos(){
  $("#tituloModal").empty();
  $("#cuerpoModal").empty();
  $("#tituloModal").append("Términos, condiciones y política de datos");
  $("#cuerpoModal").append("<iframe width='100%' height='700px' src='../assets/fup_politica_datos_personales.pdf'></iframe");
  $("#modalDialog").addClass("modal-xl");
  $("#miModal").modal("toggle");
}

function cargando(texto){
    if (!texto) {
      texto = "";
    }
    Swal.fire({
      title: 'Cargando...',
      html: "<img width='200px' src='../assets/images/loading.gif'></img><br>Por favor espere<br>"+texto,
      showConfirmButton: false,
    });
}


$(".temas-side-toggle").click(function () {
    $("#right-sidebar").slideDown(50);
    $("#right-sidebar").toggleClass("shw-rside");
});
$(".container-fluid").click(function () {
    if ($("#right-sidebar").hasClass("shw-rside")) {
      $("#right-sidebar").slideDown(50);
      $("#right-sidebar").toggleClass("shw-rside");
    }else if ($("#mensajesRapidos").hasClass("shw-rside")) {
      $("#mensajesRapidos").slideDown(50);
      $("#mensajesRapidos").toggleClass("shw-rside");
    }
});
$('.form-search').keyup(function() {
    var query = $('.form-search').val();
    $('#chatLista li a span').each(function(){
        if ($(this).text().search(new RegExp(query, "i")) < 0) {
            $('#chatLista li').has($(this)).hide();
          console.log($(this));
        } else {
            $('#chatLista li').has($(this)).show();
        }
    });
});
