<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Cursos
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_docente.php";
require_once __DIR__."/../../controller/curso_con.php";
require_once __DIR__."/../../controller/control_version_con.php";
require_once __DIR__."/../../controller/usuario_con.php";
require_once __DIR__."/../../controller/asignatura_con.php";
require_once __DIR__."/../../controller/tipo_curso_con.php";
require_once __DIR__."/../../controller/programa_con.php";
require_once __DIR__."/../../controller/facultad_con.php";
require_once __DIR__."/../../controller/componentes.php";
$comp = new Componentes();
$control_version_con = new Control_Version_Con();
$layout = new Layout_Docente();
$curso = new Curso_Con();
$programa = new Programa_Con();
$facultad = new Facultad_Con();
$docente = new Usuario_Con();
$version = new Control_Version_Con();
$asignatura = new Asignatura_Con();
$cursos = $curso->verDocente();
$programas = $programa->ver();
$facultades = $facultad->ver();
$tipo_curso = new Tipo_Curso_Con();
$layout->metaHead(
    "
    <link rel='stylesheet' type='text/css' href='../assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css'>
    <link rel='stylesheet' type='text/css' href='../assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css'>
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Cursos</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item active">Cursos</li>
                </ol>
            </div>
        </div>
    </div>

<?php if(@$_GET['success'] == 1) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i> Curso registrado con exito.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php elseif(@$_GET['success'] == 2) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i> Curso actualizado con exito
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Cursos aprobados</h4>
        </div>
        <div class="card-body">
            <div class="row">
            <div class="form-group col-md-6 ">
                  <label>Filtrar por facultad</label>
                  <?php
                    echo $comp->combobox($facultades, "id_facultad", "nombre", @$_GET['id_facultad']);
                    ?>
              </div>
              <div class="form-group col-md-6">
                  <label>Filtrar por programa</label>
                  <?php if (@$_GET['id_facultad']) :
                        echo $comp->combobox($programas, "id_programa", "nombre", @$_GET['id_programa']);
                        ?>
                  <?php else: ?>
                    <select class="form-control custom-select" id="id_programa" name="" >
                      <option value="">-- Selecciona la facultad --</option> 
                    </select>
                  <?php endif; ?>
              </div>
            </div>
            <div class="form-group">
              <button onclick="filtros();" class="btn btn-block btn-success"><i class="fa fa-check"></i> Aplicar filtros</button>
            </div>
            <hr>
            <h6 class="card-subtitle">Lista de cursos académicos</h6>
                  <div class="table-responsive m-t-40">
                      <table id="data_table"
                          class="display nowrap table table-hover table-striped table-bordered"
                          cellspacing="0" width="100%">
                          <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Versiones</th>
                            <th>Diseñador actúal</th>
                            <th>Programa</th>
                            <th>Facultad</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Versiones</th>
                            <th>Diseñador</th>
                            <th>Programa</th>
                            <th>Facultad</th>
                            <th>Opciones</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php 
                    foreach ($cursos as $cur) {
                        $versiones = $control_version_con->conteo($cur->id_control_version);
                        if (isset($_GET['id_programa']) && @$_GET['id_programa'] != $cur->programa->id_programa) {
                            continue;
                        } elseif (isset($_GET['id_facultad']) && @$_GET['id_facultad'] != $cur->facultad->id_facultad) {
                            continue;
                        } elseif (isset($_GET['id_facultad']) && isset($_GET['id_programa'])  
                            && @$_GET['id_facultad'] != $cur->facultad->id_facultad  
                            && @$_GET['id_programa'] != $cur->programa->id_programa
                        ) {
                            continue;
                        } elseif ($cur->ultima_revision) {
                            continue;
                        } elseif (!$cur->fecha_fin) {
                          continue;
                        }
                        $asig = $asignatura->ver($cur->id_asignatura);
                        $proceso = "proceso_inicial?id_curso=".$cur->id_curso;
                        if ($cur->aprobacion_inicial_coordinador) {
                            if ($cur->aprobacion_revisor) {
                                $proceso = "ver_curso?id_curso=".$cur->id_curso;
                            } else {
                                $proceso = "proceso_revision?id_curso=".$cur->id_curso;
                            }
                        }
                        echo"
                          <tr>
                              <td>$asig->nombre</td>
                              <td>$asig->codigo</td>
                              <td>".$versiones."</td>
                              <td>".$cur->diseñador->nombres." ".$cur->diseñador->apellidos."</td>
                              <td>".$cur->programa->nombre."</td>
                              <td>".$cur->facultad->nombre."</td>
                              <td>
                                <a class='btn btn-primary' href='ver_curso?id_curso=".$cur->id_curso."'><i class='ti-eye'></i> Ver curso</a>
                              </td>
                          </tr>
                          ";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <!-- This is data table -->
    <script src='../assets/node_modules/datatables.net/js/jquery.dataTables.min.js'></script>
    <script src='../assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js'></script>
    <!-- start - This is for export functionality only -->
    <script src='https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js'></script>
    <script src='../assets/js/dtable.js'></script>
    <script src=\"javascript/cursos.js\"></script>
    "
);
?>
