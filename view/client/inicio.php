<?php
require_once __DIR__."/../../controller/sesion.php";
require_once __DIR__."/../layout/layout_docente.php";
require_once __DIR__."/../../controller/componentes.php";
require_once __DIR__."/../../config/config.php";
require_once __DIR__."/../../controller/rol_con.php";
require_once __DIR__."/../../controller/pais_con.php";
require_once __DIR__."/../../controller/estado_con.php";
require_once __DIR__."/../../controller/ciudad_con.php";
require_once __DIR__."/../../controller/tipo_evaluacion_con.php";
$pais = new pais_con();
$ciudad = new ciudad_con();
$estado = new estado_con();
$rol = new rol_con();
// $roles = $rol->ver();
$conf = new Config();
$layout = new Layout_Docente();
$comp = new Componentes();
$tipo_evaluacion = new tipo_evaluacion_con();
$paises = $pais->ver();
;
$tipos_documento = $tipo_evaluacion->ver();
$layout->metaHead();
$layout->navBar();
$layout->sideBar();
?>

<?php
$layout->footer();
?>
<script>
  window.location.href = "cursos";
</script>
