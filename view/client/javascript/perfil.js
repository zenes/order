function cambioFoto(){
  $("#cuerpoModal").empty();
  $("#tituloModal").empty();
  $("#tituloModal").append("Foto de perfíl");
  var cuerpo = "\
  <div class='text-center'>\
    <canvas hidden id='canvas' width='500' height='500'></canvas>\
    <img class='img-circle' height='150' width='150' id='img' src='"+$("#img_perfil").attr("src")+"'>\
  </div>\
  <br><input id='inp' class='btn btn-info btn-block' type='file' accept='image/*'></input>'\
    <textarea hidden id='b64'></textarea><br>\
  <button onclick='editarFoto();' class='btn btn-success btn-block'><i class='ti-check'></i> Guardar</button>\
  ";
  $("#cuerpoModal").append(cuerpo);
  $("#miModal").modal("toggle");

  document.getElementById("inp").addEventListener("change", leerFoto);
}

/*
 * Esta funcion lee las imagenes de subida y las transforma a base64
 */
function leerFirma() {
  if (this.files && this.files[0]) {
    if(this.files[0].size > 128000){
      alert("Error, el archivo debe ser menor a 128KB");
      return false;
    }
    var FR= new FileReader();
    FR.addEventListener("load", function(e) {
      document.getElementById("img").src       = e.target.result;
      document.getElementById("b64").innerHTML = e.target.result;
    }); 
    FR.readAsDataURL( this.files[0] );
  }
}
/*
 * Esta funcion lee las imagenes de subida y las transforma a base64
 */
function leerFoto() {
  if (this.files && this.files[0]) {
    if(this.files[0].size > 128000){
      alert("Error, el archivo debe ser menor a 128KB");
      return false;
    }
    var FR= new FileReader();
    FR.readAsDataURL( this.files[0] );
    FR.onload = (event) => { // called once readAsDataURL is completed
      this.url = event.target.result;
      const canvas = document.getElementById('canvas');
      canvas.width=$("#canvas").attr("width");
      canvas.height=$("#canvas").attr("height");
      const ctx = canvas.getContext('2d');
      var image = new Image();
      image.src = this.url;
      image.onload = function(){
        ctx.drawImage(image, 0, 0, image.width,    image.height,     // source rectangle
                     0, 0, canvas.width, canvas.height); // destination rectangle
        $("#img").attr("src", canvas.toDataURL());
        $("#b64").val(canvas.toDataURL());
      }
    }
  }
}
function editarFoto(){
  var foto = $("#b64").val();
  if (foto == "") {
    Swal.fire("Por favor suba una foto");
    return false;
  }
  var canvas = document.createElement("canvas");                  
  canvas.width  = 500;
  canvas.height = 500;
  canvas.getContext("2d").scale(500,500);
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar la imagen de perfíl se va a cambiar",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
          async:true,
          url: "../controller/usuario_con/editarFoto",
          data: {
              foto: foto,
          },
          error: function (jqXHR, exception) {
          alert(jqXHR.status);
          },
          success: function (result) {
            if(result == 1){
                Swal.fire({
                  title: 'Foto actualizada con exito',
                  text: "La foto se ha modificado de manera exitosa",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                    window.location.href = "perfil";
                  }
                })
              }else
                  Swal.fire("La foto no ha podido ser modificada");
          }
      });
    }
  })
}

function cambioFirma(){
  $("#cuerpoModal").empty();
  $("#tituloModal").empty();
  $("#tituloModal").append("Firma digital");
  var cuerpo = "\
  <div class='text-center'>\
    <img id='img' width='256px'>\
  </div>\
  <br><input id='inp' class='btn btn-info btn-block' type='file' accept='image/*'></input>'\
    <textarea hidden id='b64'></textarea><br>\
  <button onclick='editarFirma();' class='btn btn-success btn-block'><i class='ti-check'></i> Guardar</button>\
  ";
  $("#cuerpoModal").append(cuerpo);
  $("#miModal").modal("toggle");
  document.getElementById("inp").addEventListener("change", leerFirma);
}

function editarFirma() {
  var firma = $("#b64").val();
  if (foto == "") {
    Swal.fire("Por favor suba el archivo");
    return false;
  }
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar la firma registrada se eliminara ",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
          async:true,
          url: "../controller/usuario_con/editarFirma",
          data: {
              firma: firma,
          },
          error: function (jqXHR, exception) {
          alert(jqXHR.status);
          },
          success: function (result) {
            console.log(result);
            if(result == 1){
                Swal.fire({
                  title: 'Firma modificada con exito',
                  text: "La firma se ha podido modificar de manera exitosa",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                    window.location.href = "perfil";
                  }
                })
              }else
                  Swal.fire("La firma no ha podido ser modificada");
          }
      });
    }
  })
}
