var competencias = JSON.parse($("#competencias_especificas").val());
$('.competencia').keydown(function(){
  competencias[0] = $("#competenciam1").val();
  competencias[1] = $("#competenciam2").val();
  competencias[2] = $("#competenciam3").val();
  competencias[3] = $("#competenciam4").val();
  $("#competencias_especificas_view").empty();
  $("#competencias_especificas_view").html("\
    <ul class='list-group'>\
      <li class='list-group-item'>"+competencias[0]+"</li>\
      <li class='list-group-item'>"+competencias[1]+"</li>\
      <li class='list-group-item'>"+competencias[2]+"</li>\
      <li class='list-group-item'>"+competencias[3]+"</li>\
    </ul>\
    ");
  $("#competencias_especificas").val(JSON.stringify(competencias));
});

$(".rev").toggle();
var timer = null;
$('input[type=text], textarea').keydown(function(){
       clearTimeout(timer); 
       timer = setTimeout(guardar, 5000)
});

function guardar(){
  $.ajax({
        type: "POST",
        async:true,
        url: "../controller/curso_con/procesoInicial",
        data: $("#formulario").serialize(),
        error: function (jqXHR, exception) {
        alert(jqXHR.status);
        },
        success: function (result) {
          $('#notificacion').toast('show');
        }
      });
}

$('#formulario').submit(function() {
  event.preventDefault();
  guardar();
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar no podra seguír editando el curso",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
      if (result.value) {
        $(this).unbind('submit').submit(); 
      }else{
        return false;
      }
  })
});

function habilitable(id, estado){
  guardar();
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar el estado de la habilitación cambiara",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
      if (result.value) {
          $.ajax({
            type: "POST",
            async:true,
            url: "../controller/curso_con/habilitable",
            data: {
              id_curso: id,
              estado: estado
            },
            error: function (jqXHR, exception) {
            alert(jqXHR.status);
            },
            success: function (result) {
              if(result == '1'){
                Swal.fire({
                  title: 'Cambio realizado con exito',
                  text: "El estado de la habilitación del curso se cambío exitosamente",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                     location.reload(); 
                  }
                })
              }else
                  Swal.fire("No se pudo cambiar el estado de la habilitación del curso");
            }
          });
      }else{
        return false;
      }
  })
}
