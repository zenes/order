function filtros(){
  var url = "";
  var id_programa = $("#id_programa").val();
  var id_facultad = $("#id_facultad").val();
  if (id_programa != "" && id_facultad != "") {
    url+="id_programa="+id_programa+"&id_facultad="+id_facultad;
  }else if (id_programa == "" && id_facultad != ""){
    url+="id_facultad="+id_facultad;
  }else if (id_facultad == "" && id_programa != "") {
    url+="id_programa="+id_programa;
  }
  window.location.href = "cursos?"+url;
}
function verVersiones(id){
  $.ajax({
      type: "POST",
      async:true,
      dataType: "json",
      url: "../controller/curso_con/verVersionesJson",
      data: {
          id: id,
      },
      error: function (jqXHR, exception) {
      alert(jqXHR.status);
      },
      success: function (result) {
        var ultId = 0;
        $("#cuerpoModal").empty();
        $("#tituloModal").empty();
        $("#tituloModal").append("Lista de versiones");
        var tabla = "<table id='data_table2'\
                    class='display nowrap table table-hover table-bordered'\
                    cellspacing='0' width='100%'>\
                    <thead>\
                        <tr>\
                            <th>Version</th>\
                            <th>Docente</th>\
                            <th>Fecha de creación</th>\
                            <th>Opciones</th>\
                        </tr>\
                    </thead>\
                    <tfoot>\
                        <tr>\
                            <th>Version</th>\
                            <th>Docente</th>\
                            <th>Fecha de creación</th>\
                            <th>Opciones</th>\
                        </tr>\
                    </tfoot>\
                    <tbody>";
        for (var i = 0; i < result.length; i++) {
          ultId = result[i].id_curso;
          tabla+="\
          <tr>\
              <td>"+(i+1)+"</td>\
              <td>"+result[i].docente.nombres+" "+result[i].docente.apellidos+"</td>\
              <td>"+result[i].fecha_inicio+"</td>\
              <td>\
                <div class=\"dropdown show\">\
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\
                        <i class='fa fa-cogs'></i> Ver opciones\
                    </a>\
                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">\
                      <a class='dropdown-item' href='ver_curso?id_curso="+result[i].id_curso+"&version="+(i+1)+"'><i class='ti-eye'></i> Ver</a>\
                      <a class='dropdown-item' href='registro_curso?update=1&id_curso="+result[i].id_curso+"'><i class='ti-pencil'></i> editar</a>\
                      <button class='dropdown-item' onclick='eliminarVersion("+result[i].id_curso+");'><i class='fa fa-trash'></i> Eliminar</button>\
                    </div>\
                </div>\
              </td>\
          </tr>";
        }
        tabla+=" </tbody>\
                </table>";
        $("#cuerpoModal").append(tabla);
        $("#cuerpoModal").append("\
        <hr>\
        <button class='btn btn-block btn-primary'\
        onclick='curso_nueva_version("+ultId+");'>\
        <i class='ti-plus'></i> Agregar nueva versión</button>\
        ");
        $("#modalDialog").addClass("modal-xl");
        $("#miModal").modal("toggle");
        $('#data_table2').dataTable().fnDestroy();
        $('#data_table2').DataTable({
            dom: 'Bfrtip',
            responsive: true
        });
      }
  });
}
function curso_nueva_version(id) {
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar se creara una nueva versión del curso",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
          async:true,
          url: "../controller/curso_con/nuevaVersion",
          data: {
              id: id,
          },
          error: function (jqXHR, exception) {
          alert(jqXHR.status);
          },
          success: function (result) {
            if(result == 1){
                Swal.fire({
                  title: 'Se ha creado la nueva versión con éxito',
                  text: "Versión creada con éxito",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                    $("#miModal").modal("toggle");
                  }
                })
              }else
              Swal.fire("No se pudo crear una nueva versión, \
              recuerde que para crear una nueva versión la última versión debe estar\
              finalizada");
          }
      });
    }
  })
}
$('#id_facultad').on('change', function() {
    $.ajax({
        type: "POST",
        async:true,
        dataType: "json",
        url: "../controller/programa_con/verFacultadJson",
        data: {
            id: this.value
        },
        error: function (jqXHR, exception) {
          alert(jqXHR.status+" "+exception);
        },
        success: function (result) {
          $("#id_programa").empty();
          $("#id_programa").append("<option value=''>--Seleccionar--</option>");
          for (var i = result.length - 1; i >= 0; i--) {
              $("#id_programa").append("<option value='"+result[i].id_programa+"'> "+
                  result[i].nombre+"</option>");
          }
        }
    });
});

function eliminarTodo(id){
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar el curso y sus versiones seran eliminados permanentemente ",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
          async:true,
          url: "../controller/curso_con/eliminarTodo",
          data: {
              id: id,
          },
          error: function (jqXHR, exception) {
          alert(jqXHR.status);
          },
          success: function (result) {
            if(result == 1){
                Swal.fire({
                  title: 'Curso eliminado con exito',
                  text: "El curso ha sido eliminado",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                    window.location.href = "cursos";
                  }
                })
              }else
                  Swal.fire("El curso no se ha podido eliminar");
          }
      });
    }
  })
}

function eliminarVersion(id){
  Swal.fire({
    title: '¿Esta seguro de querer continuar?',
    text: "Al continuar la version sera eliminada permanentemente ",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Deseo continuar',
    cancelButtonText: 'Cancelar',
  }).then((result) => {
    if (result.value) {
      $.ajax({
          type: "POST",
          async:true,
          url: "../controller/curso_con/eliminarVersion",
          data: {
              id: id,
          },
          error: function (jqXHR, exception) {
          alert(jqXHR.status);
          },
          success: function (result) {
            if(result == 1){
                Swal.fire({
                  title: 'Version eliminada con exito',
                  text: "La version ha sido eliminada",
                  icon: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Continuar'
                }).then((result) => {
                  if (result.value) {
                    window.location.href = "cursos";
                  }
                })
              }else
                  Swal.fire("La version no se ha podido eliminar");
          }
      });
    }
  })
}
