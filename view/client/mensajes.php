<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Notificaciones
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_docente.php";
require_once __DIR__."/../../controller/notificacion_con.php";
require_once __DIR__."/../../controller/componentes.php";
$comp = new Componentes();
$layout = new Layout_Docente();
$notificacion = new Notificacion_Con();
$notificaciones = $notificacion->verUsuario();
$layout->metaHead(
    "
    <link src=\"../assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css\">
    <link src=\"../assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css\">
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Mensajes</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item active">Mensajes</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <a href="#notificacion" class="btn btn-success float-right"><i class="fa fa-plus"></i> Crear notificació</a>
            <h4 class="card-title">Mensajes</h4>
            <h6 class="card-subtitle">Lista de mensajes</h6>
            <div class="table-responsive m-t-40">
                <table id="data_table"
                    class="display nowrap table table-hover table-striped table-bordered"
                    cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id notificación</th>
                            <th>Envía</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id notificación</th>
                            <th>Envía</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Opciones</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php 
                    foreach ($notificaciones as $notificacion) {
                        $estado = "<i class='fa fa-circle text-success'></i> Visto";
                        if (!$notificacion->visto) {
                            $estado = "<i class='fa fa-circle text-danger'></i> Sin ver";
                        }
                        echo"
                          <tr>
                              <td>$notificacion->id_notificacion</td>
                              <td>$notificacion->envia</td>
                              <td>$estado</td>
                              <td>$notificacion->fecha_envio</td>
                              <td>
                                  <button class='btn btn-success' onclick='verNotificacion($notificacion->id_notificacion, $notificacion->id_envia);'><i class='fa fa-eye'></i> Ver</button>
                                  <button class='btn btn-info' onclick='responder($notificacion->id_envia);'><i class='fa fa-envelope'></i> Responder</button>
                              </td>
                          </tr>
                          ";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/datatables.net/js/jquery.dataTables.min.js\"></script>
    <script src=\"../assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js\"></script>
    <!-- start - This is for export functionality only -->
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js\"></script>
    <script src=\"../assets/js/dtable.js\"></script>
    "
);
?>
