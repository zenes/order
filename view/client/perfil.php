<?php
require_once __DIR__."/../../controller/sesion.php";
require_once __DIR__."/../layout/layout_docente.php";
require_once __DIR__."/../../controller/componentes.php";
require_once __DIR__."/../../config/config.php";
require_once __DIR__."/../../controller/rol_con.php";
require_once __DIR__."/../../controller/pais_con.php";
require_once __DIR__."/../../controller/usuario_con.php";
require_once __DIR__."/../../controller/estado_con.php";
require_once __DIR__."/../../controller/ciudad_con.php";
require_once __DIR__."/../../controller/tipo_evaluacion_con.php";
$pais = new pais_con();
$ciudad = new ciudad_con();
$estado = new estado_con();
$rol = new rol_con();
$usuario_con = new Usuario_Con();
$usuario = $usuario_con->ver($_SESSION['id']);
// $roles = $rol->ver();
$conf = new Config();
$layout = new Layout_Docente();
$comp = new Componentes();
$tipo_evaluacion = new tipo_evaluacion_con();
$paises = $pais->ver();
;
$tipos_documento = $tipo_evaluacion->ver();
$layout->metaHead();
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                                <li class="breadcrumb-item active">Perfil</li>
                            </ol>
                        </div>
                    </div>
                </div>
<?php if (@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio o creación del usuario
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php elseif(@$_GET['success'] == 1) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i>
           Perfíl actualizado con éxito. 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php elseif(@$_GET['success'] == 2) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i>
           Contraseña actualizada con éxito. 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php elseif(@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio de la contraseña del usuario
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                    <center class="m-t-30"> 
                        <img src="<?php echo $_SESSION['foto']?>" id="img_perfil" class="img-circle" width="150">
                        <h4 class="card-title m-t-10"><?php echo $_SESSION['usuario']?></h4>
                        <h6 class="card-subtitle"><?php echo $_SESSION['rol_actual']?> en univida</h6>
                          <button onclick="cambioFoto();" class="btn btn-secondary btn-block"><i class="fa fa-image"></i> Cambiar foto de perfíl</button>
                    </center>
                    </div>
                    <div>
                        <hr> </div>
                    <div class="card-body"> 
                    <small class="text-muted">Correo electrónico </small>
                    <h6><?php echo $usuario->email?></h6>
                    <small class="text-muted">Documento de identidad</small>
                    <h6><?php echo $usuario->documento?></h6>
                    <small class="text-muted p-t-30 db">Número telefónico </small>
                    <h6>+57 <?php echo $usuario->telefono?></h6>
                    <small class="text-muted p-t-30 db">Dirección</small>
                    <h6><?php echo $usuario->direccion?></h6>
                    <small class="text-muted p-t-30 db">Sexo</small>
                    <h6><?php 
                    if ($usuario->genero == 2) {
                        echo "Mujer";
                    } elseif ($usuario->genero == 1) {
                        echo "Hombre";
                    } else {
                        echo "Otro";
                    }?></h6>
                    <small class="text-muted p-t-30 db">Fecha de registro</small>
                    <h6><?php echo $usuario->fecha_registro?></h6>
                    <small class="text-muted p-t-30 db">Fecha de último inicio de sesión</small>
                    <h6><?php echo $usuario->fecha_ingreso?></h6>
                    <hr>
                    <div class="text-center">
                      <?php if (!$usuario->google_user) : ?>
                        <button class="btn btn-danger"><i class="ti-google"></i> Activar inicio de sesion con google</button>
                      <?php endif; ?>
                    </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab" aria-selected="false"><i class="ti-user"></i> Perfil</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false"><i class="ti-settings"></i> Configuración</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#password" role="tab" aria-selected="false"><i class="ti-key"></i> Contraseña</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre completo</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $usuario->nombres." ".$usuario->apellidos?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Código</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $usuario->codigo ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Usuario</strong>
                                        <br>
                                        <p class="text-muted"><?php echo $usuario->usuario?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Estado de sesion con google</strong>
                                        <br>
                                        <p class="text-muted">
                                        <?php if ($usuario->google_user) : ?>
                                          <b class='bg-success'>Activo</b>
                                        <?php else: ?>
                                          <b class='bg-info text-white'>No activado</b>
                                        <?php endif; ?>
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 col-xs-6 b-r"> <strong>Firma digital</strong>
                                    <?php
                                    if ($usuario->firma) {
                                        echo "<br><div class=\"text-center\"><img src=\"".$usuario->firma."\" width=\"256px\" alt=\"Firma digital\" /></div>
                                        <br><button class=\"btn btn-info btn-block\" onclick=\"cambioFirma();\" type=\"file\"><i class=\"ti-pencil\"></i> Cambiar firma digital</button>";
                                    } else {
                                        echo "<button class=\"btn btn-warning btn-block\" onclick=\"cambioFirma();\" type=\"file\"><i class=\"ti-plus\"></i> Activar firma digital</button>";
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="<?php echo $conf->getUri("controller/usuario_con/actualizar") ?>" method="POST">
                                    <div class="form-group">
                                        <label class="col-md-12">Documento</label>
                                        <div class="col-md-12">
                                        <input type="text" placeholder="" value="<?php echo $usuario->documento?>" name="documento" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Nombres</label>
                                        <div class="col-md-12">
                                        <input type="text" placeholder="" value="<?php echo $usuario->nombres?>" name="nombres" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Apellidos</label>
                                        <div class="col-md-12">
                                        <input type="text" placeholder="" value="<?php echo $usuario->apellidos?>" name="apellidos" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                        <input type="email" placeholder="" value="<?php echo $usuario->email?>" class="form-control form-control-line" name="email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Sexo</label>
                                        <div class="col-sm-12">
                                            <select class="form-control form-control-line" name="genero">
                                                <option value="1"
                                                <?php if ($usuario->genero == 1) : ?>
                                                  selected
                                                <?php endif; ?>>Hombre</option>
                                                <option value="2"
                                                <?php if ($usuario->genero == 2) : ?>
                                                  selected
                                                <?php endif; ?>>Mujer</option>
                                                <option value="3"
                                                <?php if ($usuario->genero == 3) : ?>
                                                  selected
                                                <?php endif; ?>>Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Dirección</label>
                                        <div class="col-md-12">
                                        <input type="text" value="<?php echo $usuario->direccion?>" name="direccion" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Telefono</label>
                                        <div class="col-md-12">
                                        <input type="text" placeholder="" value="<?php echo $usuario->telefono?>" name="telefono"  class="form-control form-control-line phone-inputmask">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success btn-block"><span class="ti-check"></span> Actualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="password" role="tabpanel">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="<?php echo $conf->getUri("controller/usuario_con/editarClaveUsuario") ?>" method="POST">
                                    <div class="form-group">
                                        <label class="col-md-12">Contraseña actúal</label>
                                        <div class="col-md-12">
                                        <input type="password" placeholder="" value="" name="clave_vieja" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Contraseña nueva</label>
                                        <div class="col-md-12">
                                        <input type="password" placeholder="" name="clave_nueva1"  class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Repita la contraseña nueva</label>
                                        <div class="col-md-12">
                                        <input type="password" placeholder="" name="clave_nueva2"  class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success btn-block"><span class="ti-check"></span> Actualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>

<?php
$layout->footer(
    "
  <script src=\"../assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js\"></script>
  <script src=\"../assets/js/pages/mask.init.js\"></script>
  <script src=\"javascript/perfil.js\"></script>
"
);
?>
