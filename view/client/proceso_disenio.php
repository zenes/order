<?php
require_once __DIR__."/../../controller/sesion.php";
require_once __DIR__."/../layout/layout_docente.php";
require_once __DIR__."/../../controller/componentes.php";
require_once __DIR__."/../../controller/curso_con.php";
require_once __DIR__."/../../controller/asignatura_con.php";
require_once __DIR__."/../../controller/modulo_con.php";
require_once __DIR__."/../../controller/revision_con.php";
require_once __DIR__."/../../controller/unidad_tematica_con.php";
require_once __DIR__."/../../controller/evaluacion_con.php";
require_once __DIR__."/../../controller/usuario_con.php";
require_once __DIR__."/../../controller/tipo_curso_con.php";
require_once __DIR__."/../../controller/programa_con.php";
require_once __DIR__."/../../config/config.php";

$conf = new Config();
$layout = new Layout_Docente();
$comp = new Componentes();
$curso_con = new Curso_Con();
$usuario_con = new Usuario_Con();
$asignatura_con = new Asignatura_Con();
$modulo_con = new Modulo_Con();
$tipo_curso_con = new Tipo_Curso_Con();
$programa_con = new Programa_Con();
$unidad_tematica_con = new Unidad_Tematica_Con();
$revision_con = new Revision_Con();
$evaluacion_con = new Evaluacion_Con();

$curso = $curso_con->ver($_GET['id_curso']);
if ($curso->ultima_revision) {
    header(
        "location:../../".$_SESSION['rol_actual']."/cursos"
    );
}
$revisiones = $revision_con->verCurso($_GET['id_curso']);


$revBasicos = array();
/*
 * Revisiones de modulo
 */
$nombreModulo = array();
$aprendizajeModulo = array();
$introduccionModulo = array();
/*
 * Revisiones de unidad
 */
$nombreUnidad = array();
$introduccionUnidad = array();
$objetivoUnidad = array();
$recursosUnidad = array();
$recursosExtraUnidad = array();
$criterioEvaluacion = array();
$descripcionEvaluacion = array();
$referencias = array();
foreach ($revisiones as $rev) {
    if ($rev->id_tipo_revision > 5 && $rev->id_tipo_revision < 15) {
        array_push($revBasicos, $rev);
    } elseif ($rev->id_tipo_revision > 14 && $rev->id_tipo_revision < 18) {
        switch ($rev->id_tipo_revision) {
        case 15:
            array_push($nombreModulo, $rev);
            break;
        case 16:
            array_push($aprendizajeModulo, $rev);
            break;
        default:
            array_push($introduccionModulo, $rev);
            break;
        }
    } elseif ($rev->id_tipo_revision > 17) {
        switch ($rev->id_tipo_revision) {
        case 18:
            array_push($nombreUnidad, $rev);
            break;
        case 19:
            array_push($introduccionUnidad, $rev);
            break;
        case 20:
            array_push($objetivoUnidad, $rev);
            break;
        case 21:
            array_push($recursosUnidad, $rev);
            break;
        case 22:
            array_push($recursosExtraUnidad, $rev);
            break;
        case 23:
            array_push($criterioEvaluacion, $rev);
            break;
        case 24:
            array_push($descripcionEvaluacion, $rev);
            break;
        default:
            array_push($referencias, $rev);
            break;
        }
    }
}

$modulos = $modulo_con->verCurso($_GET['id_curso']);
$docente = $usuario_con->ver($curso->id_docente);
$revisor = $usuario_con->ver($curso->id_revisor);
$asignatura = $asignatura_con->ver($curso->id_asignatura);
$programa = $programa_con->ver($asignatura->id_programa);
$coordinador = $usuario_con->ver($programa->id_coordinador);
$tipo_curso = $tipo_curso_con->ver($curso->id_tipo_curso);

$revision = 0;
$cantidad_revisiones = $modulos[0];
$cantidad_revisiones = json_decode($cantidad_revisiones->aprendizaje, true);
if (!$cantidad_revisiones) {
    $cantidad_revisiones = 1;
} else {
    $cantidad_revisiones = count($cantidad_revisiones);
}
if (empty($_GET['revision']) || @!is_numeric($_GET['revision'])) {
    $revision = $cantidad_revisiones-1;
} else {
    $revision = $_GET['revision']-1;
}
$competencias = json_decode($curso->competencias_especificas)[$revision];
$layout->metaHead(
    "
    <link href=\"../assets/css/pages/tab-page.css\" rel=\"stylesheet\">
    <link href=\"../assets/css/pages/progressbar-page.css\" rel=\"stylesheet\">
"
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Proceso de diseño del curso</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="cursos">Cursos</a></li>
                    <li class="breadcrumb-item active">Ver curso</li>
                </ol>
            </div>
        </div>
    </div>
<?php if (@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio en el curso, contacte al adminitrador
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>
  <div class="row">
    <div class="card col-md-12">
        <div class="card-body">
            <h4 class="card-title">Diseño instruccional del curso</h4>
            <h6 class="card-subtitle">Proceso de diseño del contenido del curso</h6>
          <!-- .row -->
          <div class="row text-justified m-t-10">
              <div class="col-md-6 border-right">
                  <strong>Nombre del curso:</strong> <?php echo $asignatura->nombre;?><br>
                  <strong>Tipo de curso:</strong> <?php echo $tipo_curso->nombre;?><br>
                  <strong>Código:</strong> <?php echo $asignatura->codigo;?><br>
              </div>
              <div class="col-md-6">
                  <strong>Diseñador:</strong> <?php echo $revisor->nombres." ".$revisor->apellidos;?><br>
                  <strong>Docente:</strong> <?php echo $docente->nombres." ".$docente->apellidos;?><br>
                  <strong>Coordinador:</strong> <?php echo $coordinador->nombres." ".$coordinador->apellidos;?><br>
              </div>
          </div>
          <hr>
          <div class="row text-justified m-t-10">
              <div class="col-md-6 border-right">
                  <strong>Horas de acompañamiento directo:</strong> <?php echo $curso->horas_directo;?><br>
                  <strong>Horas de trabajo independiente:</strong> <?php echo $curso->horas_independiente;?><br>
                  <strong>Duración:</strong> <?php echo $curso->duracion;?> semanas<br>
                  <strong>Créditos:</strong> <?php echo $asignatura->creditos;?><br>
              </div>
              <div class="col-md-6">
                  <strong>Habilitable:</strong>
                  <?php
                    if ($curso->habilitable) {
                        echo "
                        <button onclick='habilitable(".$_GET['id_curso'].", 0);' class='btn btn-success'><i class='fa fa-check'></i> Sí (click para cambiar)</button>
                      ";
                    } else {
                        echo "
                        <button onclick='habilitable(".$_GET['id_curso'].", 1);' class='btn btn-danger'><i class='fa fa-times'></i> No (click para cambiar)</button>
                      ";
                    } 
                    ?>
                    <br>
                  <strong>Revisíon actúal:</strong> <?php echo $revision+1?>
                  <br>
                  <strong>Progreso:</strong> <?php echo $curso_con->progreso($curso->id_curso)?>%
                  <div class="progress">
                      <div class="progress-bar bg-success" style="width: <?php echo $curso_con->progreso($curso->id_curso)?>%; height:6px;" role="progressbar"></div>
                  </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
    </div>
    <form action="<?php echo $conf->getUri("controller/curso_con/enviarRevisionDisenio")?>" id="formulario" method="POST">
    <input type="number" name="id_curso" hidden readonly value="<?php echo $_GET['id_curso']?>"></input>
      <div class="card col-md-12">
          <div class="card-body">
              <h4 class="card-title">Información de diseño para el desarrollo del curso</h4>
              <h6 class="card-subtitle">En este punto se diseñan todas las caracteristicas del curso</h6>
              <hr>
              <!-- Nav tabs -->
              <div class="vtabs customvtab">
                  <ul class="nav nav-tabs tabs-vertical" role="tablist">
                      <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#intro" role="tab"><span class="hidden-sm-up"><i class="ti-blackboard"></i></span> <span class="hidden-xs-down">Introducción</span> </a> </li>
                      <?php
                        $i=0;
                        foreach ($modulos as $modulo) {
                            $i++;
                            echo" 
                          <li class=\"nav-item\"> <a class=\"nav-link\" data-toggle=\"tab\" href=\"#modulo$i\" role=\"tab\"><span class=\"hidden-sm-up\"><i class=\"ti-book\"></i></span> <span class=\"hidden-xs-down\">Módulo $i</span></a> </li>
                          ";
                        }
                        ?>
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                      <div class="tab-pane active" id="intro" role="tabpanel" >
                              <h3>Datos básicos</h3>
                              <h4>Información básica del curso</h4>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Perfíl del docente:</b>
                                  <hr>
                                  <?php if ($revBasicos[0]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_perfil').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Ingrese el perfíl del docente:</label>
                                        <textarea id="perfil_docente" 
                                        <?php if ($revBasicos[0]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Datos del perfíl del docente." name="perfil_docente" class="form-control" rows="4"><?php echo json_decode($curso->perfil_docente)[$revision] ?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: El docente debera cumplir con las competencias necesarias para la dirección del curso ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_perfil" name="rev_perfil" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[0]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Presentación:</b>
                                  <hr>
                                  <?php if ($revBasicos[1]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_presentacion').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Ingrese la presentación del curso:</label>
                                        <textarea id="presentacion" name="presentacion" 
                                        <?php if ($revBasicos[1]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Datos de la presentación del curso." class="form-control" rows="4"><?php echo json_decode($curso->presentacion)[$revision]?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: El curso xxxxx esta diseñado con el fin de enseñar las técnicas y prácticas apropiadas para la elaboración de ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_presentacion" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[1]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Conocimientos previos</b>
                                  <hr>
                                  <?php if ($revBasicos[2]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_conocimientos').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Ingrese los conocimientos previos requeridos por el curso:</label>
                                        <textarea id="conocimiento_previo" name="conocimiento_previo" 
                                        <?php if ($revBasicos[2]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Datos de los conocimientos previos requeridos para poder realizar el curso." class="form-control" rows="4"><?php echo json_decode($curso->conocimiento_previo)[$revision]?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: El estudiante debera tener competencias básicas o intermedias en el manejo de las TICs para el desarrollo de ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_conocimientos" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[2]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Requerimientos tecnológicos</b>
                                  <hr>
                                  <?php if ($revBasicos[3]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_requerimientos').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>ingrese que requerimientos tecnológicos requiere el curso:</label>
                                        <textarea id="requerimientos" name="requerimientos" 
                                        <?php if ($revBasicos[3]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Materiales tecnológicos necesarios para el desarrollo del curso" class="form-control" rows="4"><?php echo json_decode($curso->requerimientos)[$revision]?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: El estudiante y el docente deberan tener acceso a una computadora o dispositivo móvil con acceso a internet ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_requerimientos" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[3]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Objetivo general</b>
                                  <hr>
                                  <?php if ($revBasicos[4]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_objetivo').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Ingrese el objetivo general del curso:</label>
                                        <textarea id="objetivo_general" name="objetivo_general" 
                                        <?php if ($revBasicos[4]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Datos generales de lo que busca el curso" class="form-control" rows="4"><?php echo json_decode($curso->objetivo_general)[$revision]?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: El curso busca que el estudiante se encuentre en la capacidad de analizar los contextos que lo rodean para el desarrollo de ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_objetivo" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[4]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Justficación</b>
                                  <hr>
                                  <?php if ($revBasicos[5]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_justificacion').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Ingrese la justificación del curso:</label>
                                        <textarea id="justificacion" name="justificacion" 
                                        <?php if ($revBasicos[5]->aprobacion) : ?>
                                        readonly
                                        <?php endif;?>
                                        placeholder="Contexto debido al cual es necesario el curso" class="form-control" rows="4"><?php echo json_decode($curso->justificacion)[$revision]?></textarea>
                                        <span class="help-block">
                                          <small>
                                            Ejemplo: Este curso es necesario debido a que en el area xxxx se requieren profesionales con xxxx competencias para llevar a cabo ...
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_justificacion" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[5]->observacion)[$revision]?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Competencias específicas</b>
                                  <hr>
                                  <?php if ($revBasicos[6]->aprobacion) : ?>
                                    <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                  <?php else: ?>
                                    <button type="button" onclick="$('#rev_competencias').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-10" align="justify">
                                    <div class="form-group">
                                        <label>Lista de las competencias específicas desarrolladas en el curso:</label>
                                        <ul id="competencias_especificas_view" class="list-group">
                                        <?php
                                        foreach ($competencias as $competencia) {
                                            echo "<li class='list-group-item'>$competencia</li>";
                                        }
                                        ?>
                                        </ul>
                                        <span class="help-block">
                                          <small>
                                            Esta lista se genera automáticamente.
                                          </small>
                                        </span>
                                        <hr>
                                        <textarea id="rev_competencias" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[6]->observacion)[$revision]?></textarea>
                                        <textarea id="competencias_especificas" hidden readonly><?php echo json_encode($competencias)?></textarea>
                                    </div>
                                </div>
                              </div>
                              <hr>
                              <?php if ($curso->habilitable) : ?>
                                      <hr>
                                      <div class="row">
                                        <div class="col-md-2">
                                          <b>Actividad de habilitación</b>
                                          <hr>
                                          <?php if ($revBasicos[7]->aprobacion) : ?>
                                            <button type="button" class="btn btn-success float-right"><i class="ti-check"></i> Aprobado</button>
                                          <?php else: ?>
                                            <button type="button" onclick="$('#rev_habilitacion').toggle();" class="btn btn-info float-right"><i class="ti-info"></i> Ver revisión</button>
                                          <?php endif; ?>
                                        </div>
                                        <div class="col-md-10" align="justify">
                                          <div class="form-group">
                                              <label>Ingrese la actividad de habilitación planteada para el curso:</label>
                                              <textarea id="habilitacion" name="habilitacion" 
                                              <?php if ($revBasicos[7]->aprobacion) : ?>
                                              readonly
                                              <?php endif;?>
                                              placeholder="Datos de el ejercicio de habilitación del curso" class="form-control" rows="4"><?php echo json_decode($curso->habilitacion)[$revision]?></textarea>
                                              <span class="help-block">
                                                <small>
                                                  Ejemplo: El estudiante debera realizar un taller con ...
                                                </small>
                                              </span>
                                            <hr>
                                            <textarea id="rev_habilitacion" class="rev form-control" readonly disabled rows="6"><?php echo json_decode($revBasicos[7]->observacion)[$revision]?></textarea>
                                          </div>
                                        </div>
                                      </div>
                              <?php endif;?>
                              <hr>
                              <div class="row">
                                <div class="col-md-2">
                                  <b>Metodología</b>
                                </div>
                                <div class="col-md-10" align="justify">
                                  <?php echo $curso->metodologia?>
                                </div>
                              </div>
                              <hr>
                              <p>Recuerde, no es necesario guardar, el sistema guardara todos los cambios de manera automática luego de 15 segundos de inactividad, igualmente si se quiere guardar manualmente pueden hacer uso del siguiente botón</p>
                      </div>
                      <?php
                        $i=0;
                        $n=0;
                        foreach ($modulos as $modulo) {
                            $unidades = $unidad_tematica_con->verModulo($modulo->id_modulo);
                            $i++;
                            $pagina = " 
                          <div class=\"tab-pane\" id=\"modulo$i\" role=\"tabpanel\">
                            <ul class=\"nav nav-tabs\" id=\"myTab".$i."\" role=\"tablist\">";
                            $j = 0;
                            $pagina.="
                            <li class=\"nav-item\"> 
                              <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#presentacionm".$i."\" role=\"tab\" aria-controls=\"presentacionm".$i."\" aria-expanded=\"true\"><span class=\"hidden-sm-up\"><i class=\"ti-clipboard\"></i></span>
                                <span class=\"hidden-xs-down\">
                                Presentación
                                </span>
                              </a>
                            </li>";
                            foreach ($unidades as $unidad){
                                $j++;
                                $pagina.= "
                                    <li class=\"nav-item clear\"> <a class=\"nav-link\" data-toggle=\"tab\" href=\"#unidad".$j."m".$i."\" role=\"tab\" aria-controls=\"unidad".$j."m".$i."\" aria-expanded=\"true\"><span class=\"hidden-sm-up\"><i class=\"ti-tablet\"></i></span> <span class=\"hidden-xs-down\">Unidad ".$j."</span></a> </li>
                                  "; 
                            }
                            $aprendizaje = json_decode($modulo->aprendizaje, true);
                            $aprendizaje = $aprendizaje[$revision];
                            $introduccion = json_decode($modulo->introduccion, true);
                            $introduccion = $introduccion[$revision];
                            $pagina.="</ul>
                          <div class=\"tab-content tabcontent-border p-20\" id=\"myTabContent".$i."\">
                            <div role=\"tabpanel\" class=\"tab-pane fade show active\" id=\"presentacionm".$i."\" aria-labelledby=\"presentacionm".$i."\">
                              <div class=\"row\">
                                <div class=\"col-md-2\">
                                  <b>Nombre:</b>
                                  <hr>";
                            if ($nombreModulo[$i-1]->aprobacion) {
                                $pagina.="
                                <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                              ";
                            } else {
                                $pagina.="
                                  <button type=\"button\" onclick=\"$('#rev_nombrem".$i."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                              ";
                            }
                                $pagina.="
                                </div>
                                <div class=\"col-md-10\" align=\"justify\">
                                  <div class=\"form-group\">
                                      <label>Ingrese el nombre del módulo:</label>
                                      <textarea id=\"nombrem".$i."\" ";
                            if ($nombreModulo[$i-1]->aprobacion) {
                                $pagina.=" readonly ";
                            }
                            $pagina.="placeholder=\"Nombre del módulo\" name=\"nombrem".$i."\" class=\"form-control\" rows=\"1\">".$modulo->nombre."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: Conceptos básicos de ...
                                          </small>
                                        </span>
                                  </div>
                                  <div id=\"rev_nombrem".$i."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($nombreModulo[$i-1]->observacion)[$revision]."</textarea>
                                  </div>
                                </div>
                              </div>
                              <hr>
                              <div class=\"row\">
                                <div class=\"col-md-2\">
                                  <b>Aprendizaje:</b>
                                  <hr>";
                            if ($aprendizajeModulo[$i-1]->aprobacion) {
                                $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                            } else {
                                $pagina.="
                                      <button type=\"button\" onclick=\"$('#rev_aprendizajem".$i."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                            }
                            $pagina.="
                                </div>
                                <div class=\"col-md-10\" align=\"justify\">
                                  <div class=\"form-group\">
                                      <label>Ingrese el aprendizaje del módulo:</label>
                                      <textarea id=\"aprendizajem".$i."\" ";
                            if ($aprendizajeModulo[$i-1]->aprobacion) {
                                $pagina.=" readonly ";
                            }
                            $pagina.="placeholder=\"Información detallada de lo que se quiere enseñar con el módulo\" name=\"aprendizajem".$i."\" class=\"form-control\" rows=\"5\">".$aprendizaje."</textarea>
                                      <span class=\"help-block\">
                                        <small>
                                          Ejemplo: En este módulo se va a enseñar ...
                                        </small>
                                      </span>
                                  </div>
                                  <div id=\"rev_aprendizajem".$i."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($aprendizajeModulo[$i-1]->observacion)[$revision]."</textarea>
                                  </div>
                                </div>
                              </div>
                              <hr>
                              <div class=\"row\">
                                <div class=\"col-md-2\">
                                  <b>Introducción:</b>
                                  <hr>";
                            if ($introduccionModulo[$i-1]->aprobacion) {
                                $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                            } else {
                                $pagina.="
                                      <button type=\"button\" onclick=\"$('#rev_introduccionm".$i."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                            }
                            $pagina.="
                                </div>
                                <div class=\"col-md-10\" align=\"justify\">
                                  <div class=\"form-group\">
                                      <label>Ingrese la introducción al módulo:</label>
                                      <textarea id=\"introduccionm".$i."\" ";
                            if ($introduccionModulo[$i-1]->aprobacion) {
                                $pagina.=" readonly ";
                            }
                            $pagina.="placeholder=\"Introducción detallada al módulo\" name=\"introduccionm".$i."\" class=\"form-control\" rows=\"5\">".$introduccion."</textarea>
                                      <span class=\"help-block\">
                                        <small>
                                          Ejemplo: Este módulo contiene la información ...
                                        </small>
                                      </span>
                                  </div>
                                  <div id=\"rev_introduccionm".$i."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($introduccionModulo[$i-1]->observacion)[$revision]."</textarea>
                                  </div>
                                </div>
                              </div>
                              <hr>
                              <div class=\"row\">
                                <div class=\"col-md-2\">
                                  <b>Competencía específica:</b>
                                </div>
                                <div class=\"col-md-10\" align=\"justify\">
                                  <div class=\"form-group\">
                                      <label>Ingrese la competencía que se busca con el módulo:</label>
                                      <textarea id=\"competenciam".$i."\" name=\"competenciam".$i."\" ";
                            if ($revBasicos[6]->aprobacion) {
                                $pagina.=" readonly ";
                            }
                                      $pagina.="placeholder=\"Competencia que se quiere enseñar con este módulo\" class=\"form-control competencia\" rows=\"5\">".$competencias[$i-1]."</textarea>
                                      <span class=\"help-block\">
                                        <small>
                                          Ejemplo: En este módulo se busca que el estudiante aprenda los conceptos fundamentales para ...
                                          (La competencía específica se ve reflejada en los datos básicos del curso, por lo tanto no tiene revisión individual)
                                        </small>
                                      </span>
                                  </div>
                                </div>
                              </div>
                            </div>";
                            $j = 0;
                            foreach ($unidades as $unidad) {
                                $j++;
                                $pagina.="
                            <div class=\"tab-pane fade\" id=\"unidad".$j."m".$i."\" role=\"tabpanel\" aria-labelledby=\"unidad1m".$i."\">
                                <div class=\"row\">
                                  <div class=\"col-md-2\">
                                    <b>Titulo: </b>
                                    <hr>";
                                if ($nombreUnidad[$n]->aprobacion) {
                                    $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                                } else {
                                    $pagina.="
                                    <button type=\"button\" onclick=\"$('#rev_nombreu".$i."m".$j."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                                }
                                $pagina.="
                                  </div>
                                  <div class=\"col-md-10\" align=\"justify\">
                                    <div class=\"form-group\">
                                        <label>Ingrese el título de la unidad temática:</label>
                                        <textarea id=\"titulou".$j."m".$i."\" ";
                                if ($nombreUnidad[$n]->aprobacion) {
                                    $pagina.=" readonly ";
                                }
                                $pagina.="placeholder=\"Titulo de la únidad temática\" name=\"titulou".$j."m".$i."\" class=\"form-control\" rows=\"2\">".$unidad->nombre."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: Titulo x unidad ".$j." ...
                                          </small>
                                        </span>
                                    </div>
                                  <div id=\"rev_nombreu".$i."m".$j."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($nombreUnidad[$n]->observacion)[$revision]."</textarea>
                                  </div>
                                  </div>
                                </div>
                                <hr>
                                <div class=\"row\">
                                  <div class=\"col-md-2\">
                                    <b>Introducción</b>
                                    <hr>";
                                if ($introduccionUnidad[$n]->aprobacion) {
                                    $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                                } else {
                                    $pagina.="
                                    <button type=\"button\" onclick=\"$('#rev_introduccionu".$i."m".$j."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                                }
                                $pagina.="
                                  </div>
                                  <div class=\"col-md-10\" align=\"justify\">
                                    <div class=\"form-group\">
                                        <label>Ingrese la introducción a la únidad tématica</label>
                                        <textarea id=\"introduccionu".$j."m".$i."\" ";
                                if ($introduccionUnidad[$n]->aprobacion) {
                                    $pagina.=" readonly ";
                                }
                                $pagina.="placeholder=\"Introducción a la únidad tématica\" name=\"introduccionu".$j."m".$i."\" class=\"form-control\" rows=\"4\">".json_decode($unidad->introduccion,true)[$revision]."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: Esta unidad se encuentra enfocada dentro de un contexto ...
                                          </small>
                                        </span>
                                    </div>
                                  <div id=\"rev_introduccionu".$i."m".$j."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($nombreUnidad[$n]->observacion, true)[$revision]."</textarea>
                                  </div>
                                  </div>
                                </div>
                                <hr>
                                <div class=\"row\">
                                  <div class=\"col-md-2\">
                                    <b>Objetivo</b>
                                    <hr>";
                                if ($objetivoUnidad[$n]->aprobacion) {
                                    $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                                } else {
                                    $pagina.="
                                    <button type=\"button\" onclick=\"$('#rev_objetivou".$i."m".$j."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                                }
                                $pagina.="
                                  </div>
                                  <div class=\"col-md-10\" align=\"justify\">
                                    <div class=\"form-group\">
                                        <label>Ingrese el objetivo a la únidad tématica</label>
                                        <textarea id=\"objetivou".$j."m".$i."\" ";
                                if ($objetivoUnidad[$n]->aprobacion) {
                                    $pagina.=" readonly ";
                                }
                                $pagina.="placeholder=\"Objetivo de la unidad tématica\" name=\"objetivou".$j."m".$i."\" class=\"form-control\" rows=\"4\">".json_decode($unidad->objetivo, true)[$revision]."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: En esta unidad se busca ...
                                          </small>
                                        </span>
                                    </div>
                                  <div id=\"rev_objetivou".$i."m".$j."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($objetivoUnidad[$n]->observacion)[$revision]."</textarea>
                                  </div>
                                  </div>
                                </div>
                                <hr>
                                <div class=\"row\">
                                  <div class=\"col-md-2\">
                                    <b>Recursos de la únidad</b>
                                    <hr>";
                                if ($recursosUnidad[$n]->aprobacion) {
                                    $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                                } else {
                                    $pagina.="
                                    <button type=\"button\" onclick=\"$('#rev_recursosu".$i."m".$j."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                                }
                                $pagina.="
                                  </div>
                                  <div class=\"col-md-10\" align=\"justify\">
                                    <div class=\"form-group\">
                                        <label>Ingrese los recursos que se van a utilizar en la únidad</label>
                                        <textarea id=\"recursosu".$j."m".$i."\" ";
                                if ($recursosUnidad[$n]->aprobacion) {
                                    $pagina.=" readonly ";
                                }
                                $pagina.="placeholder=\"Recursos de la unidad tématica\" name=\"recursosu".$j."m".$i."\" class=\"form-control\" rows=\"4\">".json_decode($unidad->recursos, true)[$revision]."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: Lecturas, Libros, URL
                                          </small>
                                        </span>
                                    </div>
                                  <div id=\"rev_recursosu".$i."m".$j."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($recursosUnidad[$n]->observacion)[$revision]."</textarea>
                                  </div>
                                  </div>
                                </div>
                                <hr>
                                <div class=\"row\">
                                  <div class=\"col-md-2\">
                                    <b>Recursos y lecturas extra</b>
                                    <hr>";
                                if ($recursosExtraUnidad[$n]->aprobacion) {
                                    $pagina.="
                                      <button type=\"button\" disabled class=\"btn btn-success float-right\"><i class=\"ti-check\"></i> Aprobado</button>
                                    ";
                                } else {
                                    $pagina.="
                                    <button type=\"button\" onclick=\"$('#rev_recursosextrau".$i."m".$j."').toggle();\" class=\"btn btn-info float-right\"><i class=\"ti-info\"></i> Ver revisión</button>
                                    ";
                                }
                                $pagina.="
                                  </div>
                                  <div class=\"col-md-10\" align=\"justify\">
                                    <div class=\"form-group\">
                                        <label>Ingrese lecturas y libros extra para usarse en la únidad</label>
                                        <textarea id=\"recursosextrau".$j."m".$i."\" ";
                                if ($recursosExtraUnidad[$n]->aprobacion) {
                                    $pagina.=" readonly ";
                                }
                                $pagina.="placeholder=\"Recursos extra de la unidad tématica\" name=\"recursosextrau".$j."m".$i."\" class=\"form-control\" rows=\"4\">".json_decode($unidad->recursos_extra, true)[$revision]."</textarea>
                                        <span class=\"help-block\">
                                          <small>
                                            Ejemplo: Lecturas, Libros, URL
                                          </small>
                                        </span>
                                    </div>
                                  <div id=\"rev_recursosextrau".$i."m".$j."\" class=\"rev form-group\">
                                      <label>Revisión del diseñador:</label>
                                      <textarea readonly disabled class=\"form-control\" rows=\"5\">".json_decode($recursosExtraUnidad[$n]->observacion)[$revision]."</textarea>
                                  </div>
                                  </div>
                                </div>
                            </div>
                              ";
                                $n++;
                            }
                            $pagina.="
                              <br>
                              <hr>
                              <p>Recuerde, no es necesario guardar, el sistema guardara todos los cambios de manera automática luego de 5 segundos de inactividad, igualmente si se quiere guardar manualmente pueden hacer uso del siguiente botón</p>
                            </div>
                          </div>";
                            echo $pagina;
                        }
                        ?>
                        <hr>
                        <button onclick="guardar();" type="button" class="btn btn-success btn-block">Guardar</button>
                        <hr>
                        <button type="submit" class="btn btn-primary btn-block"><i class="ti-envelope"></i> Envíar a revisión</button>
                  </div>
              </div>
          </div>
            <div id="notificacion" class="toast position-fixed" data-delay="5000" style="bottom: 0; right: 0;">
              <div class="toast-header">
                <img src="..." class="rounded mr-2" alt="...">
                <strong class="mr-auto">¡Alerta!</strong>
                <small>Información</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="toast-body">
                Se han guardado los datos del curso
              </div>
            </div>
      </div>
    </form>
  </div>
</div>
<?php
$layout->footer(
    "
    <script src=\"javascript/proceso_disenio.js\"></script>
  "
);
?>
