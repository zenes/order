function show(id){
  $.ajax({
      type: "POST", async:true,
      dataType: "json",
      url: "../controller/user_con/readJson",
      data: {
          id: id,
      },
      error: function (jqXHR, exception) {
      alert(jqXHR.status);
      },
      success: function (result) {
        console.log(result);
        if (result.gender == 1) {
          result.gender = "Mujer"
        }else if(result.gender == 0){
          result.gender = "Hombre"
        }else{
          result.gender = "Otro"
        }
        var roles='';
        $("#cuerpoModal").empty();
        $("#tituloModal").empty();
        $("#tituloModal").append("Información del usuario");
        $("#cuerpoModal").append("\
        <p><b>Nombre: </b>"+result.name+"</p>\
        <p><b>Genero: </b>"+result.gender+"</p>\
        <p><b>Carro id: </b>"+result.id_truck+"</p>\
        <p><b>Email: </b>"+result.email+"</p>\
        <p><b>Fecha de nacimiento: </b>"+result.date+"</p>\
        <a class='btn btn-primary'\
        href='user_reg?update=1&id_usuario="+result.id+"'>\
        <i class='ti-pencil'></i> Editar</a>");
        $("#miModal").modal("show");
      }
  });
}

function del(id){
Swal.fire({
  title: '¿Esta seguro de querer continuar?',
  text: "Al continuar el usuario sera eliminado ",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Deseo continuar',
  cancelButtonText: 'Cancelar',
}).then((result) => {
  if (result.value) {
    $.ajax({
        type: "POST",
        async:true,
        url: "../controller/user_con/delete",
        data: {
            id: id,
        },
        error: function (jqXHR, exception) {
        alert(jqXHR.status);
        },
        success: function (result) {
          if(result == 1){
              Swal.fire({
                title: 'Usuario eliminado con exito',
                text: "El usuario ha sido eliminado",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Continuar'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "users";
                }
              })
            }else
                Swal.fire("El usuario no se ha podido eliminar");
        }
    });
  }
})
}
