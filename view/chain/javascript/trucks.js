function del(id){
Swal.fire({
  title: '¿Esta seguro de querer continuar?',
  text: "Al continuar el carro sera eliminado ",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Deseo continuar',
  cancelButtonText: 'Cancelar',
}).then((result) => {
  if (result.value) {
    $.ajax({
        type: "POST",
        async:true,
        url: "../controller/truck_con/delete",
        data: {
            id: id,
        },
        error: function (jqXHR, exception) {
        alert(jqXHR.status);
        },
        success: function (result) {
          if(result == 1){
              Swal.fire({
                title: 'Carro eliminado con exito',
                text: "El carro ha sido eliminado",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Continuar'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "trucks";
                }
              })
            }else
                Swal.fire("El carro no se ha podido eliminar");
        }
    });
  }
})
}
