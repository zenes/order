function del(id){
Swal.fire({
  title: '¿Esta seguro de querer continuar?',
  text: "Al continuar la cadena sera eliminada ",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Deseo continuar',
  cancelButtonText: 'Cancelar',
}).then((result) => {
  if (result.value) {
    $.ajax({
        type: "POST",
        async:true,
        url: "../controller/chain_con/delete",
        data: {
            id: id,
        },
        error: function (jqXHR, exception) {
        alert(jqXHR.status);
        },
        success: function (result) {
          if(result == 1){
              Swal.fire({
                title: 'Cadena eliminada con exito',
                text: "La cadena ha sido eliminada",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Continuar'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "chains";
                }
              })
            }else
                Swal.fire("La cadena no se ha podido eliminar");
        }
    });
  }
})
}
