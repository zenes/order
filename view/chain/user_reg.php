<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Registro_User
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_chain.php";
require_once __DIR__."/../../controller/user_con.php";
require_once __DIR__."/../../controller/truck_con.php";
$user = new User_Con();
$truck = new Truck_Con();
$layout = new Layout_Chain();

$trucks = $truck->readChain();

$tipo = "create";
if (@$_GET['update']) {
    $info = $user->read($_GET['id']); 
    $tipo = "update";
} 
$layout->metaHead(
    "
    <link href=\"../assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/select2/dist/css/select2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/switchery/dist/switchery.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/multiselect/css/multi-select.css\" rel=\"stylesheet\" type=\"text/css\" />
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Registro de empleado</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="users">Empleados</a></li>
                    <li class="breadcrumb-item active">Registro de empleado</li>
                </ol>
            </div>
        </div>
    </div>

<?php if (@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio en los datos del empleado
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>

    <form id="form1" action="<?php echo $conf->getUri("controller/user_con/".$tipo)?>" method="POST">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Formulario de registro</h4>
                        <h6 class="card-subtitle">Formulario de registro de un nuevo empleado</h6>
                        <input type="number" name="id" 
                        hidden readonly value="<?php echo @$info->id?>">
                        <div class="form-group row">
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Nombre</label>
                              <input type="text" class="form-control" required name="name" value="<?php echo @$info->name?>" id="name"
                              alt="Ingresa el nombre del empleado" placeholder="Nombre">
                              <small class="form-text text-muted">Ingresa el nombre de la empleado.</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Email</label>
                              <input type="email" class="form-control" required name="email" value="<?php echo @$info->email?>" id="email"
                              alt="Ingresa el correo del empleado" placeholder="Email">
                              <small class="form-text text-muted">Ingresa el correo del empleado.</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Carro</label>
                              <select class="form-control" name="id_truck">
                              <?php
                                foreach ($trucks as $truck) {
                                 echo "
                                  <option value=\"$truck->id\">$truck->name</option> 
                                  ";
                                }
                              ?>
                              </select>
                              <small class="form-text text-muted">Seleccione el manager de la cadena</small>
                            </div>
                            <input hidden readonly name="id_role" value="3">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Celular</label>
                              <input type="text" class="form-control" required name="cellphone" value="<?php echo @$info->cellphone?>" id="cellphone"
                              alt="Ingresa el numero de celular del empleado" placeholder="Número de calular">
                              <small class="form-text text-muted">Ingresa el número de celular del empleado.</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Fecha de nácimiento</label>
                              <input type="date" class="form-control" required name="date" value="<?php echo @$info->date?>" id="date">
                              <small class="form-text text-muted">Selecciona la fecha de nacimiento</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Genero</label>
                              <select class="form-control" name="gender">
                                <option value="0">Hombre</option> 
                                <option value="1">Mujer</option>
                                <option value="2">Otro</option>
                              </select>
                              <small class="form-text text-muted">Selecciona tu genero</small>
                            </div>
                        </div>
                        <?php if (!isset($_GET['update'])) : ?>
                          <div class="form-group row">
                              <div class="col-md-12">
                                <label for="exampleInputEmail1">Contraseña</label>
                                <input type="password" class="form-control" required name="password" value="<?php echo @$info->password?>" id="password"
                                alt="Ingresa la contraseña" placeholder="Contraseña">
                                <small class="form-text text-muted">Ingresa la contraseña.</small>
                              </div>
                          </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit"> Guardar </button>
    </form>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js\"></script>
    <script src=\"../assets/js/pages/mask.init.js\"></script>
    <script src=\"../assets/node_modules/switchery/dist/switchery.min.js\"></script>
    <script src=\"../assets/node_modules/select2/dist/js/select2.full.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js\"></script>
    <script src=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/dff/dff.js\" type=\"text/javascript\"></script>
    <script type=\"text/javascript\" src=\"../assets/node_modules/multiselect/js/jquery.multi-select.js\"></script>
    <script type=\"text/javascript\" src=\"../assets/js/switchery.js\"></script>
    "
);
?>
<script>
document.getElementById("photo").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
