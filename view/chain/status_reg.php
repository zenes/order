<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Registro_Status
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_admin.php";
require_once __DIR__."/../../controller/status_con.php";
$status = new Status_Con();
$layout = new Layout_Admin();

$tipo = "create";
if (@$_GET['update']) {
    $info = $status->read($_GET['id']); 
    $tipo = "update";
} 
$layout->metaHead(
    "
    <link href=\"../assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/select2/dist/css/select2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/switchery/dist/switchery.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/multiselect/css/multi-select.css\" rel=\"stylesheet\" type=\"text/css\" />
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Registro de estado de restaurantes</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="status">Estados</a></li>
                    <li class="breadcrumb-item active">Registro estado</li>
                </ol>
            </div>
        </div>
    </div>

<?php if (@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio en los datos de la estado
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>

    <form action="<?php echo $conf->getUri("controller/status_con/".$tipo)?>" method="POST">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Formulario de registro</h4>
                        <h6 class="card-subtitle">Formulario de registro de una nueva estado</h6>
                        <input type="number" name="id" 
                        hidden readonly value="<?php echo @$info->id?>">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input type="text" class="form-control" required name="name" value="<?php echo @$info->name?>" id="name"
                            alt="Ingresa el nombre de la estado" placeholder="Nombre">
                            <small class="form-text text-muted">Ingresa el nombre de la estado.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Descripción</label>
                            <textarea class="form-control" rows="3" required name="description" value="" id="description"
                            placeholder="Descripción"><?php echo @$info->description?></textarea>
                            <small class="form-text text-muted">Ingresa la descripción de la estado de restaurantes.</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit"> Guardar </button>
    </form>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js\"></script>
    <script src=\"../assets/js/pages/mask.init.js\"></script>
    <script src=\"../assets/node_modules/switchery/dist/switchery.min.js\"></script>
    <script src=\"../assets/node_modules/select2/dist/js/select2.full.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js\"></script>
    <script src=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/dff/dff.js\" type=\"text/javascript\"></script>
    <script type=\"text/javascript\" src=\"../assets/node_modules/multiselect/js/jquery.multi-select.js\"></script>
    <script type=\"text/javascript\" src=\"../assets/js/switchery.js\"></script>
    "
);
?>
<script>
document.getElementById("photo").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
