<?php
/**
 * PHP Version 7.4.3
 *
 * @category Layout
 * @package  Layout_Admin
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/layout_global.php";
require_once __DIR__."/../../config/config.php";
require_once __DIR__."/../../controller/sesion.php";
/**
 * Esta clase se encarga del Layout (esqueleto html) del administrador
 *
 * @category Layout
 * @package  Layout_Admin
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Layout_Admin
{
    use Layout_Global;
    private $_config;
    private $_pageName;
    private $_cssRoute;
    private $_jsRoute;
    private $_nodeModules;
    private $_images;
    private $_favicon;

    /**
     * Este es el metodo constructor de la clase.
     * Aquí se inician algunas variables con las rutas para
     * los assets (js, css, imagenes, iconos y librerias -node_modules-).
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return None
     */ 
    public function __construct()
    {
        $this->_config = new Config();
        $this->_pageName = str_replace(".php", "", basename($_SERVER['PHP_SELF']));
        $this->_cssRoute = $this->_config->getUri("assets/css/");
        $this->_jsRoute = $this->_config->getUri("assets/js/");
        $this->_nodeModules = $this->_config->getUri("assets/node_modules/");
        $this->_favicon = $this->_config->getUri("assets/images/favicon.ico");
        $this->_images = $this->_config->getUri("assets/images/");
    }

    /**
     * Esta funcion se encarga de imprimir la información de la
     * cabecera html <head> y de llamar estilos css y bootstrap.
     * 
     * @param string $extra es una variable que recibe importaciones
     *                      extra de css o html, en este caso lo 
     *                      implemente para cuando en una pagína aparte 
     *                      se necesite agregar estilos para 
     *                      objetos como datatables o las alertas 
     *                      ya que solo se llaman en pocas ocasiones.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function metaHead($extra = null)
    {
        ob_start();
        if ($_SESSION['rol'] != "admin") {
            header("location:".$this->_config->getUri("controller/logout"));
        }
        // ver layout_global.php
        $this->_metaHead($extra);
    }

    /**
     * Esta funcion se encarga de imprimir la información de la
     * barra de navegación superior de las pagínas html.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function navBar()
    {
        // ver Layout_global
        $this->_navBar("skin-megna-dark");
    }
    /**
     * Esta funcion se encarga de imprimir la información de la
     * barra de navegación izquierda de las pagínas html.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function sideBar()
    {
        echo "
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class=\"left-sidebar\">
            <!-- Sidebar scroll-->
            <div class=\"scroll-sidebar\">
                <!-- Sidebar navigation-->
                <nav class=\"sidebar-nav\">
                    <ul id=\"sidebarnav\">
                        <li class=\"nav-small-cap\">--- ADMINISTRADOR</li>
                        <li> <a class=\"waves-effect waves-dark\" href=\"home\"><i class=\"icon-home\"></i><span class=\"hide-menu\">Inicio</span></a>
                        </li>
                        <li> <a class=\"has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"ti-user\"></i><span class=\"hide-menu\">Usuarios</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse\">
                                <li><a href=\"users\"><i class=\"icon-eye\"></i> Ver</a></li>
                                <li><a href=\"user_reg\"><i class=\"icon-plus\"></i> Agregar</a></li>
                            </ul>
                        </li>
                        <li> <a class=\"has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"ti-blackboard\"></i><span class=\"hide-menu\">Cadenas</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse\">
                                <li><a href=\"chains\"><i class=\"icon-eye\"></i> Ver</a></li>
                                <li><a href=\"chain_reg\"><i class=\"icon-plus\"></i> Agregar</a></li>
                            </ul>
                        </li>
                        <li> <a class=\"has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"ti-book\"></i><span class=\"hide-menu\">Estados</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse\">
                                <li><a href=\"status\"><i class=\"icon-eye\"></i> Ver</a></li>
                                <li><a href=\"status_reg\"><i class=\"icon-plus\"></i> Agregar</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class=\"page-wrapper\">
        ";
    }
    /**
     * Esta funcion se encarga de imprimir la información del
     * pie de pagina html <footer> y además se encarga de la 
     * importación de los archivos javascript.
     * 
     * @param string $extra es una variable que recibe importaciones
     *                      extra de css o javascript, en este caso lo 
     *                      implemente para cuando en una pagína
     *                      necesite agregar scripts js para 
     *                      objetos como datatables, ajax o las alertas 
     *                      ya que solo se llaman en pocas ocasiones.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function footer($extra = null)
    {
        // ver layout_global
        $this->_footer($extra);
    }
}
?>
