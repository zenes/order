<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Layout
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
/**
 * Este trait tiene todas las funciones de tipo post de la clase CiudadCon
 *
 * @category Controlador
 *
 * @package CiudadCon
 * @author  José camilo Jiménez <jmilo@protonmaill.con>
 * @license MIT
 * @link    https://pbear.xyz
 */ 
trait Layout_Global
{
    /**
     * Esta funcion se encarga de imprimir la información de la
     * cabecera html <head> y de llamar estilos css y bootstrap.
     * 
     * @param string $extra es una variable que recibe importaciones
     *                      extra de css o html, en este caso lo 
     *                      implemente para cuando en una pagína aparte 
     *                      se necesite agregar estilos para 
     *                      objetos como datatables o las alertas 
     *                      ya que solo se llaman en pocas ocasiones.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    private function _metaHead($extra = null)
    {
        echo "
            <!DOCTYPE html>
            <html lang=\"en\">

            <head>
                <meta charset=\"utf-8\">
                <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta name=\"description | Administrador\" content=\"".$this->_config->getDescripcion()."\">
                <meta name=\"".$this->_config->getDesarrollador()."\" content=\"".$this->_config->getAppName()."\">
                <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"".$this->_favicon."\">
                <title>".$this->_config->getAppName()." | ".$this->_pageName."</title>
                <link href=\"".$this->_nodeModules."morrisjs/morris.css\" rel=\"stylesheet\">
                <link href=\"".$this->_nodeModules."toast-master/css/jquery.toast.css\" rel=\"stylesheet\">
                <link href=\"".$this->_cssRoute."style.min.css\" rel=\"stylesheet\">
                <link href=\"".$this->_cssRoute."pages/dashboard1.css\" rel=\"stylesheet\">
                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
                <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
                <![endif]-->
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css\">
                <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons-wind.min.css\">
                <!-- Custom CSS -->
        ".$extra;
    }
    /**
     * Esta funcion se encarga de imprimir la información de la
     * barra de navegación superior de las pagínas html.
     *
     * Colores disponibles:
     *
     * skin-default (ambar)
     * skin-green (verde)
     * skin-red (rojo)
     * skin-blue (azul)
     * skin-purple (purpura)
     * skin-megna (cyan)
     *
     * @param string $color es una variable con el color del perfil
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    private function _navBar($color = null)
    {
        echo "</head>
        <body class=\"".$color." fixed-layout\">
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <div class=\"preloader\">
                <div class=\"loader\">
                    <div class=\"loader__figure\"></div>
                    <p class=\"loader__label\">".$this->_config->getAppName()."</p>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Main wrapper - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <div id=\"main-wrapper\">
                <!-- ============================================================== -->
                <!-- Topbar header - style you can find in pages.scss -->
                <!-- ============================================================== -->
                <header class=\"topbar\">
                    <nav class=\"navbar top-navbar navbar-expand-md navbar-dark\">
                        <!-- ============================================================== -->
                        <!-- Logo -->
                        <!-- ============================================================== -->
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"inicio\">
                                <!-- logo icon --><b>
                                    <!--you can put here icon as well // <i class=\"wi wi-sunset\"></i> //-->
                                    <!-- light logo icon -->
                                </b>
                                <span class='hidden-xs' style='font-size: 130%'>
                                    <span class='font-bold' style=''>The | </span> Ôrder
                                </span>
                                <!--end logo icon -->
                            </a>
                        </div>
                        <!-- ============================================================== -->
                        <!-- End Logo -->
                        <!-- ============================================================== -->
                        <div class=\"navbar-collapse\">
                            <!-- ============================================================== -->
                            <!-- toggle and nav items -->
                            <!-- ============================================================== -->
                            <ul class=\"navbar-nav mr-auto\">
                                <!-- This is  -->
                                <li class=\"nav-item\"> <a class=\"nav-link nav-toggler d-block d-md-none waves-effect waves-dark\" href=\"javascript:void(0)\"><i class=\"ti-menu\"></i></a> </li>
                                <li class=\"nav-item\"> <a class=\"nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark\" href=\"javascript:void(0)\"><i class=\"icon-menu\"></i></a> </li>
                                <!-- ============================================================== -->
                                <!-- Search -->
                                <!-- ============================================================== -->
<!--
                                <li class=\"nav-item\">
                                    <form class=\"app-search d-none d-md-block d-lg-block\">
                                        <input type=\"text\" class=\"form-control\" placeholder=\"Search & enter\">
                                    </form>
                                </li>
-->
                            </ul>
                            <!-- ============================================================== -->
                            <!-- User profile and search -->
                            <!-- ============================================================== -->
                            <ul class=\"navbar-nav my-lg-0\">
                                <!-- ============================================================== -->
                                <!-- User Profile -->
                                <!-- ============================================================== -->
                                <li class=\"nav-item dropdown u-pro\">
                                    <a class=\"nav-link dropdown-toggle waves-effect waves-dark profile-pic\" href=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><img src=\"".$this->_config->getUri("/assets/images/users/1.webp")."\" alt=\"user\" class=\"\"> <span class=\"hidden-md-down\">".$_SESSION['user']."</span> </a>
                                    <div class=\"dropdown-menu dropdown-menu-right animated flipInY\">
                                        <!-- text-->
                                        <div class=\"dropdown-divider\"></div>
                                        <!-- text-->
                                        <a href=\"".$this->_config->getUri('/controller/logout')."\" class=\"dropdown-item\"><i class=\"fa fa-power-off\"></i> Cerrar Sesión</a>
                                        <!-- text-->
                                    </div>
                                </li>
                                <li class='nav-item temas-side-toggle'> <a class='nav-link  waves-effect waves-light' href='javascript:void(0)'><i class='fas fa-cogs'></i></a></li>
                                <!-- ============================================================== -->
                                <!-- End User Profile -->
                                <!-- ============================================================== -->
                            </ul>
                        </div>
                    </nav>
                </header>
                <!-- ============================================================== -->
                <!-- End Topbar header -->
                <!-- ============================================================== -->
        ";
    }
    /**
     * Esta funcion se encarga de imprimir la información del
     * pie de pagina html <footer> y además se encarga de la 
     * importación de los archivos javascript.
     * 
     * @param string $extra es una variable que recibe importaciones
     *                      extra de css o javascript, en este caso lo 
     *                      implemente para cuando en una pagína
     *                      necesite agregar scripts js para 
     *                      objetos como datatables, ajax o las alertas 
     *                      ya que solo se llaman en pocas ocasiones.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    private function _footer($extra = null)
    {
        echo "        
            <div id='right-sidebar' class='right-sidebar'>
                  <div class='slimscrollright'>
                      <div class='rpanel-title' style='color:#343a40; background:#fff;'> Temas de interfaz <span><i class='ti-close temas-side-toggle'></i></span> </div>
                      <div class='r-panel-body'>
                          <ul id='themecolors' class='m-t-20'>
                              <li><b>Fondo claro</b></li>
                              <li><a href='javascript:void(0)' data-skin='skin-default' class='default-theme'>1</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-green' class='green-theme'>2</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-red' class='red-theme'>3</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-blue' class='blue-theme working'>4</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-purple' class='purple-theme'>5</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-megna' class='megna-theme'>6</a></li>
                              <li class='d-block m-t-30'><b>Fondo obscuro</b></li>
                              <li><a href='javascript:void(0)' data-skin='skin-default-dark' class='default-dark-theme '>7</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-green-dark' class='green-dark-theme'>8</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-red-dark' class='red-dark-theme'>9</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-blue-dark' class='blue-dark-theme'>10</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-purple-dark' class='purple-dark-theme'>11</a></li>
                              <li><a href='javascript:void(0)' data-skin='skin-megna-dark' class='megna-dark-theme '>12</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
          <div id='mensajesRapidos' class='right-sidebar'>
              <div class='slimscrollright'>
                  <div class='rpanel-title' style='color:#343a40; background:#fff;'> <i class='fas fa-paper-plane'></i> Mensajes Rápidos <span><i class='ti-close mensajes-side-toggle'></i></span> </div>
                  <a href='mensajes' class='float-right btn btn-block btn-dark'><i class='fas fa-inbox'></i> Ir a búzon</a>
                  <hr>
                  <div class='r-panel-body'>
                    <input type='text' class='form-search form-control' placeholder='Buscar usuario'>
                    <hr>
                    <ul class='m-t-20 chatonline' id='chatLista'>
                    </ul>
                  </div>
              </div>
          </div>
        </div>
        <footer class=\"footer\">
            © ".date("Y")." ".$this->_config->getAppName()." by ".$this->_config->getDesarrollador()."
          <a href='#terminos' onclick='verTerminos()' class='float-right text-cyan'> Términos y condiciones</a>
        </footer>
    </div>
    <div id=\"miModal\" class=\"modal fade\" tabindex=\"-1\">
      <div id=\"modalDialog\" class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\">
                  <h4 class=\"modal-title\" id=\"tituloModal\"></h4>
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
              </div>
              <div id=\"cuerpoModal\"class=\"modal-body\">
              </div>
              <div class=\"modal-footer\">
                  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cerrar</button>
              </div>
          </div>
      </div>
  </div>
    <script src=\"".$this->_nodeModules."jquery/jquery-3.2.1.min.js\"></script>
    <script src=\"".$this->_nodeModules."popper/popper.min.js\"></script>
    <script src=\"".$this->_nodeModules."bootstrap/dist/js/bootstrap.min.js\"></script>
    <script src=\"".$this->_jsRoute."perfect-scrollbar.jquery.min.js\"></script>
    <script src=\"".$this->_jsRoute."waves.js\"></script>
    <script src=\"".$this->_jsRoute."sidebarmenu.js\"></script> <script src=\"".$this->_jsRoute."custom.min.js\"></script>
    <script src=\"".$this->_nodeModules."raphael/raphael-min.js\"></script>
    <script src=\"".$this->_nodeModules."morrisjs/morris.min.js\"></script>
    <script src=\"".$this->_nodeModules."jquery-sparkline/jquery.sparkline.min.js\"></script>
    <script src=\"".$this->_nodeModules."toast-master/js/jquery.toast.js\"></script>
    <script src=\"".$this->_jsRoute."dashboard1.js\"></script>
    <script src=\"".$this->_nodeModules."toast-master/js/jquery.toast.js\"></script>
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2@9'></script>
    ".$extra;
        if ($extra != "nofooter") {
            echo"<script src=\"".$this->_jsRoute."global.js\"></script>";
        }
        echo"
    </body>
    </html>
    ";
        ob_end_flush();
    }
}
?>
