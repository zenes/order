<?php
/**
 * En este archivo se guardan los elementos del diseño de las paginas
 */
require_once __DIR__."/../../config/config.php";
class LayoutIndex{
    private $config;
    private $pageName;
    private $cssRoute;
    private $favicon;
    private $jsRoute;
    private $nodeModules;
    public function __construct(){
        $this->config = new Config();
        $this->pageName = str_replace(".php","", basename($_SERVER['PHP_SELF']));
        $this->cssRoute = $this->config->getUri("assets/css/");
        $this->jsRoute = $this->config->getUri("assets/js/");
        $this->nodeModules = $this->config->getUri("assets/node_modules/");
        $this->favicon = $this->config->getUri("assets/images/favicon.png");
    }
    public function metaHead($extra = null){
        echo "
        <!DOCTYPE html>
        <html lang=\"es\">

        <head>
            <meta charset=\"utf-8\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta name=\"".$this->config->getDescripcion()."\" content=\"\">
            <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"".$this->favicon."\">
            <title>".$this->config->getAppName()." | ".$this->pageName."</title>
            <link href=\"".$this->cssRoute."pages/login-register-lock.css\" rel=\"stylesheet\">
            <link href=\"".$this->cssRoute."style.min.css\" rel=\"stylesheet\">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
            <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css\">
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons-wind.min.css\">
        </head>
        <body class=\"skin-default card-no-border\">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class=\"preloader\">
            <div class=\"loader\">
                <div class=\"loader__figure\"></div>
                <p class=\"loader__label\">".$this->config->getAppName()."</p>
            </div>
        </div>
        ";
    }
    public function footer(){
        echo"
            <div id=\"miModal\" class=\"modal fade\" tabindex=\"-1\">
              <div id=\"modalDialog\" class=\"modal-dialog\">
                  <div class=\"modal-content\">
                      <div class=\"modal-header\">
                          <h4 class=\"modal-title\" id=\"tituloModal\"></h4>
                      </div>
                      <div id=\"cuerpoModal\"class=\"modal-body\">
                      </div>
                      <div class=\"modal-footer\">
                          <button type=\"button\" onclick=\"aceptaCookies();\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Aceptar</button>
                      </div>
                  </div>
              </div>
            </div>
            <script src=\"".$this->nodeModules."jquery/jquery-3.2.1.min.js\"></script>
            <script src=\"".$this->nodeModules."popper/popper.min.js\"></script>
            <script src=\"".$this->nodeModules."bootstrap/dist/js/bootstrap.min.js\"></script>
            <script type=\"text/javascript\">
              $(function() {
                  $(\".preloader\").fadeOut();
              });
              $(function() {
                  $('[data-toggle=\"tooltip\"]').tooltip()
              });
              // ============================================================== 
              // Login and Recover Password 
              // ============================================================== 
              $('#to-recover').on(\"click\", function() {
                  $(\"#loginform\").slideUp();
                  $(\"#recoverform\").fadeIn();
              });
            </script>            
            <script>
                function aceptaCookies(){
                  document.cookie = 'cookies_syllabus=true';
                }
            </script>
        </body>
        ";
    }
}
?>
