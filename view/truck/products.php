<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Products
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_truck.php";
require_once __DIR__."/../../controller/product_con.php";
$layout = new Layout_Truck();
$product = new Product_Con();

$products = $product->readTruck();

$layout->metaHead(
    "
    <link src=\"../assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css\">
    <link src=\"../assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css\">
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Productos</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                    <li class="breadcrumb-item active">Productos</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a href="product_reg" class="btn btn-success float-right"><i class="fa fa-plus"></i> Agregar</a>
            <h4 class="card-title">Productos</h4>
            <h6 class="card-subtitle">Lista de productos del punto de venta</h6>
            <div class="table-responsive m-t-40">
                <table id="data_table"
                    class="display nowrap table table-hover table-striped table-bproducted"
                    cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Precio</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Precio</th>
                            <th>Opciones</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php 
                    foreach ($products as $product) {
                        echo"
                          <tr>
                              <td>$product->id</td>
                              <td>$product->name</td>
                              <td>$product->code</td>
                              <td>$product->price</td>
                              <td>
                                <div class=\"dropdown show\">
                                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        <i class='fa fa-cogs'></i> Ver opciones
                                    </a>
                                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                                      <button class='dropdown-item' onclick='del($product->id);'><i class='fa fa-trash'></i> Eliminar</button>
                                      <a class='dropdown-item' href='product_reg?update=1&id=$product->id'><i class='ti-pencil'></i> editar</a>
                                    </div>
                                </div>
                              </td>
                          </tr>
                          ";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <a class="" href="registro_tipo_documento&editar=1"></a>
    <button ></button>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/datatables.net/js/jquery.dataTables.min.js\"></script>
    <script src=\"../assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js\"></script>
    <!-- start - This is for export functionality only -->
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js\"></script>
    <script src=\"../assets/js/dtable.js\"></script>
    <script src=\"javascript/products.js\"></script>
    "
);
?>
