<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Status
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_admin.php";
require_once __DIR__."/../../controller/status_con.php";
$layout = new Layout_Admin();
$status = new Status_Con();
$statuses = $status->read();

$layout->metaHead(
    "
    <link src=\"../assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css\">
    <link src=\"../assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css\">
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Estados</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                    <li class="breadcrumb-item active">Estados</li>
                </ol>
            </div>
        </div>
    </div>

<?php if(@$_GET['success'] == 1) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i> Estado registrada con exito.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php elseif(@$_GET['success'] == 2) : ?>
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-check-circle"></i> Estado actualizada con exito
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>
    <div class="card">
        <div class="card-body">
            <a href="status_reg" class="btn btn-success float-right"><i class="fa fa-plus"></i> Agregar</a>
            <h4 class="card-title">Estados</h4>
            <h6 class="card-subtitle">Lista de estados de los productos</h6>
            <div class="table-responsive m-t-40">
                <table id="data_table"
                    class="display nowrap table table-hover table-striped table-bordered"
                    cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php 
                    foreach ($statuses as $status) {
                        echo"
                          <tr>
                              <td>$status->id</td>
                              <td>$status->name</td>
                              <td>
                                <div class=\"dropdown show\">
                                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        <i class='fa fa-cogs'></i> Ver opciones
                                    </a>
                                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                                      <button class='dropdown-item' onclick='del($status->id);'><i class='fa fa-trash'></i> Eliminar</button>
                                      <a class='dropdown-item' href='status_reg?update=1&id=$status->id'><i class='ti-pencil'></i> editar</a>
                                    </div>
                                </div>
                              </td>
                          </tr>
                          ";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <a class="" href="registro_tipo_documento&editar=1"></a>
    <button ></button>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/datatables.net/js/jquery.dataTables.min.js\"></script>
    <script src=\"../assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js\"></script>
    <!-- start - This is for export functionality only -->
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js\"></script>
    <script src=\"../assets/js/dtable.js\"></script>
    <script src=\"javascript/status.js\"></script>
    "
);
?>
