function del(id){
Swal.fire({
  title: '¿Esta seguro de querer continuar?',
  text: "Al continuar el producto sera eliminado ",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Deseo continuar',
  cancelButtonText: 'Cancelar',
}).then((result) => {
  if (result.value) {
    $.ajax({
        type: "POST",
        async:true,
        url: "../controller/product_con/delete",
        data: {
            id: id,
        },
        error: function (jqXHR, exception) {
        alert(jqXHR.status);
        },
        success: function (result) {
          if(result == 1){
              Swal.fire({
                title: 'Producto eliminado con exito',
                text: "El producto ha sido eliminado",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Continuar'
              }).then((result) => {
                if (result.value) {
                  window.location.href = "products";
                }
              })
            }else
                Swal.fire("El producto no se ha podido eliminar");
        }
    });
  }
})
}
