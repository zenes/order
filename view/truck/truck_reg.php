<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  Registro_Truck
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../layout/layout_chain.php";
require_once __DIR__."/../../controller/truck_con.php";
require_once __DIR__."/../../controller/user_con.php";
$truck = new Truck_Con();
$user = new User_Con();
$layout = new Layout_Chain();
$managers = $user->readManagers();

$tipo = "create";
if (@$_GET['update']) {
    $info = $truck->read($_GET['id']); 
    $tipo = "update";
} 
$layout->metaHead(
    "
    <link href=\"../assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/select2/dist/css/select2.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"../assets/node_modules/switchery/dist/switchery.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css\" rel=\"stylesheet\" />
    <link href=\"../assets/node_modules/multiselect/css/multi-select.css\" rel=\"stylesheet\" type=\"text/css\" />
    "
);
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Registro de puesto de restaurantes</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="trucks">Puestos</a></li>
                    <li class="breadcrumb-item active">Registro puesto</li>
                </ol>
            </div>
        </div>
    </div>

<?php if (@$_GET['error'] == 1) : ?>
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-times-circle"></i>
            Error al realizar el cambio en los datos de la puesto
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>

    <form action="<?php echo $conf->getUri("controller/truck_con/".$tipo)?>" method="POST" j>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Formulario de registro</h4>
                        <h6 class="card-subtitle">Formulario de registro de una nueva puesto</h6>
                        <input type="number" name="id" 
                        hidden readonly value="<?php echo @$info->id?>">
                        <input type="number" name="id_chain" 
                        hidden readonly value="<?php echo $_SESSION['chain']?>">
                        <div class="form-group row">
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Nombre</label>
                              <input type="text" class="form-control" required name="name" value="<?php echo @$info->name?>" id="name"
                              alt="Ingresa el nombre de la puesto" placeholder="Nombre">
                              <small class="form-text text-muted">Ingresa el nombre de la puesto.</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">Ciudad</label>
                              <input type="text" class="form-control" required name="city" value="<?php echo @$info->city?>" id="city"
                              alt="Ingresa la ciudad del puesto" placeholder="Ciudad">
                              <small class="form-text text-muted">Ingresa el nombre de la ciudad.</small>
                            </div>
                            <div class="col-md-4">
                              <label for="exampleInputEmail1">País</label>
                              <input type="text" class="form-control" required name="country" value="<?php echo @$info->country?>" id="country"
                              alt="Ingresa el país del puesto" placeholder="País">
                              <small class="form-text text-muted">Ingresa el nombre del país.</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Descripción</label>
                            <textarea class="form-control" rows="3" required name="description" value="" id="description"
                            placeholder="Descripción"><?php echo @$info->description?></textarea>
                            <small class="form-text text-muted">Ingresa la descripción de la puesto de restaurantes.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ubicación</label>
                            <textarea class="form-control" rows="3" required name="location" value="" id="location"
                            placeholder="Ubicación"><?php echo @$info->location?></textarea>
                            <small class="form-text text-muted">Ingresa la ubicación de la puesto de restaurantes.</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit"> Guardar </button>
    </form>
    <!-- ============================================================== -->
    <!-- End Page Content -->
</div>
<?php
$layout->footer(
    "
    <script src=\"../assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js\"></script>
    <script src=\"../assets/js/pages/mask.init.js\"></script>
    <script src=\"../assets/node_modules/switchery/dist/switchery.min.js\"></script>
    <script src=\"../assets/node_modules/select2/dist/js/select2.full.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-select/bootstrap-select.min.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js\"></script>
    <script src=\"../assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js\" type=\"text/javascript\"></script>
    <script src=\"../assets/node_modules/dff/dff.js\" type=\"text/javascript\"></script>
    <script type=\"text/javascript\" src=\"../assets/node_modules/multiselect/js/jquery.multi-select.js\"></script>
    <script type=\"text/javascript\" src=\"../assets/js/switchery.js\"></script>
    "
);
?>
<script>
document.getElementById("photo").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("preview").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
