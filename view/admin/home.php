<?php
/**
 * PHP version 7.4.3
 *
 * @category Vista
 * @package  View
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/../../controller/sesion.php";
require_once __DIR__."/../layout/layout_admin.php";
require_once __DIR__."/../../controller/chain_con.php";
$layout = new Layout_Admin();
$chain_con = new Chain_Con();
$layout->metaHead();
$layout->navBar();
$layout->sideBar();
$chains = $chain_con->readLatest();
?>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Inicio</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Info box -->
    <!-- ============================================================== -->
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <a href="users">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="d-flex no-block align-items-center">
                              <div>
                                  <h3><i class="icon-people"></i></h3>
                                  <p class="text-muted">Usuarios</p>
                              </div>
                              <div class="ml-auto">
                              <h2 class="counter text-primary">3</h2>
                              </div>
                          </div>
                      </div>
                      <div class="col-12">
                          <div class="progress">
                          <div class="progress-bar bg-primary" role="progressbar" style="width: 50%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                      </div>
                  </div>
                </a>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <a href="chains">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="d-flex no-block align-items-center">
                              <div>
                                  <h3><i class="ti-layout-grid2"></i></h3>
                                  <p class="text-muted">Cadenas</p>
                              </div>
                              <div class="ml-auto">
                              <h2 class="counter text-purple">4</h2>
                              </div>
                          </div>
                      </div>
                      <div class="col-12">
                          <div class="progress">
                              <div class="progress-bar bg-purple" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                      </div>
                  </div>
                </a>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <a href="status">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="d-flex no-block align-items-center">
                              <div>
                                  <h3><i class="ti-book"></i></h3>
                                  <p class="text-muted">Estados</p>
                              </div>
                              <div class="ml-auto">
                              <h2 class="counter text-success">5</h2>
                              </div>
                          </div>
                      </div>
                      <div class="col-12">
                          <div class="progress">
                              <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                      </div>
                  </div>
                </a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Info box -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Over Visitor, Our income , slaes different and  sales prediction -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-8 col-mg-12">
            <div class="card">
                <div class="card-body">
                    <!-- <a class="btn btn-info float-right" href="acciones"><i class="fa fa-eye"></i> Ver información completa</a> -->
                    <div class="d-flex">
                        <div>
                            <h5 class="card-title">Registro</h5>
                            <h6 class="card-subtitle">Vista de registro</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body bg-light">
                    <div class="row">
                        <div class="col-6">
                            <h3>Últimas cadenas</h3>
                            <h5 class="font-light m-t-0">últimas cadenas de restaurantes registradas</h5>
                        </div>
                        <div class="col-6 align-self-center display-6 text-right">
                            <a class="btn btn-info" href="chains"><i class="fas fa-folder-open"></i> Ver</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover no-wrap">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Nombre</th>
                                <th>Lugar</th>
                            </tr>
                        </thead>
                        <?php
                          foreach ($chains as $chain) {
                              echo "
                              <tr>
                                  <td class=\"text-center\">".$chain->id."</td>
                                  <td class=\"txt-oflo\">".$chain->name."</td>
                                  <td>".substr($chain->location, 0, 10)."...</td>
                              </tr>
                             ";
                          } 
                        ?>
                        <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Nombre</th>
                                <th>Lugar</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-4 col-md-12">
            <div class="row">
                <!-- Column -->
                <div class="col-md-12">
                    <div class="card bg-primary text-white">
                        <div class="card-body">
                            <h5 class="card-title">Carga de CPU</h5>
                            <div class="row">
                                <div class="col-6  m-t-30">
                                    <h1 class="text-white"><p id="carga"></p> %</h1>
                                    <b class="text-white">Porcentaje de uso de cpu</b></div>
                                <div class="col-6">
                                    <h1 class="m-b-"><i class="icon icon-speedometer"></i></h1>
                                    <b class="text-white">Monitor de CPU</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-12">
                    <div class="card bg-purple text-white">
                        <div class="card-body">
                            <h5 class="card-title">Memoria ram libre</h5>
                            <div class="row">
                                <div class="col-6  m-t-30">
                                    <h1 class="text-white"><p id="memoria"></p> MB</h1>
                                    <b class="text-white">Memoria total </b> <p id="memoriaTotal"></p> MB</div>
                                <div class="col-6">
                                    <h1 class="m-b-"><i class="fa fa-server"></i></h1>
                                    <b class="text-white">Monitor de memoria ram</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$layout->footer();
?>
