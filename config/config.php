<?php
/**
 * PHP Version 7.4.3
 *
 * @category Configuracion
 * @package  Config
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
/**
 * ¡¡¡¡¡¡¡ATENCIÓN!!!!!!!!
 * PONER EN 1 SOLO SI SE ESTA EN MODO DESARROLLO
 * PONER EN 0 SI SE ESTA EN PRODUCCIÓN
 */
ini_set("display_errors", 0);
/**
 * En esta clase se encuentran las configuraciones iniciales de la aplicacion
 *
 * @category Configuracion
 * @package  Config
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Config
{
    /**
     * Datos para el acceso a la base de datos
     */
    private $_db = array(
        "type" => "mysql",
        "host" => "localhost",
        "user" => "root",
        "db" => "the_order",
        "port" => "3306",
        "password" => ""
    );

    /**
     * Array con las rutas admitidas por el controlador
     * para casteo de objetos o ejecucion de metodos
     * mediante peticiones POST
     * PD: NO AGREGAR SESION Y/O LOGOUT.
     */
    private $_rutas = array(
       "router", 
       "cambio_rol", 
      //EL ROUTER SIEMPRE DEBE ESTAR
       "chain_con",
       "email",
       "role_con",
       "status_con",
       "role_con",
       "topping_con",
       "product_con",
       "truck_con",
       "user_con",
       // VIEW
       "admin",
       "chain",
       "truck",
       "client",
    );
    /**
     * Datos de email
     */
    private $_email = array(
        "server" => "smtp.gmail.com",
        "user" => "theorder_temp@gmail.com",
        "pass" => "Theorder123", //esto se ve mal, pero es temporal
        "port" => "587",
        "username" => "THE ORDER",
        "crypt" => "tls"
    );
    /**
     * Tiempo de la sesion, se recomienda 3600 (para 1 hora) 
     * o 7200 (para 2 horas)
     */
    private $_tiempoSesion = 7200;
    /**
     * Nombre del dominio de la app
     */
    private $_appName = "order";
    /**
     * Nombre del dominio o host donde se aloja la aplicacion
     * MODIFICAR TAMBIEN EN ARCHIVO .htaccess DE LA CARPETA RAIZ
     *  - Dejar en null si la aplicacion va a usar el dominio.
     *  - Poner si se va a utilizar esta app en un subdominio
     */
    private $_host = "http://localhost";
    /**
     * Descripcion de  la aplicación
     */
    private $_descripcion ="The order project";
    /**
     * Nombre de desarrollador
     */
    private $_desarrollador ="José Camilo Jiménez";

    /**
     * Este es el metodo constructor, en este caso es vacio
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return void
     */ 
    public function __construct()
    {
    }
  
    /**
     * Esta funcion retorna todas las rutas necesarias en el router
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array
     */ 
    public function getRutas()
    {
        return $this->_rutas;
    }
  
    /**
     * Esta funcion retorna el array con la conexion a la base de datos.
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array
     */ 
    public function getDb()
    {
        return $this->_db;
    }

    /**
     * Esta funcion retorna el nombre de la aplicacion
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function getAppName()
    {
        return $this->_appName;
    }
  
    /**
     * Esta funcion retorna los datos del stp para usar email en la aplicacion
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array
     */ 
    public function getEmail()
    {
        return $this->_email;
    }
  
    /**
     * Esta funcion retorna la url de la aplicacion y la 
     * concatena con una subdireccion externa
     * 
     * @param string $subUri es una variable con la subdireccion 
     *                       que se concatena con la url de la aplicacion
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function getUri($subUri="")
    {
        if (empty($this->_host)) {
            return $subUri;
        }
        return $this->_host."/".$this->_appName."/".$subUri;
    }
  
    /**
     * Esta funcion retorna la descripcion de la aplicacion
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function getDescripcion()
    {
        return $this->_descripcion;
    }
  
    /**
     * Esta funcion retorna el tiempo de sesion en segundos de la aplicacion
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return int
     */ 
    public function getTiempoSesion()
    {
        return $this->_tiempoSesion;
    }
  
    /**
     * Esta funcion retorna el nombre del desarrollador
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return string
     */ 
    public function getDesarrollador()
    {
        return $this->_desarrollador;
    }
}
?>
