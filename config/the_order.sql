-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-11-2020 a las 17:48:36
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `the_order`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `addtopping`
--

CREATE TABLE `addtopping` (
  `id` int(164) NOT NULL,
  `id_makeorder` int(128) NOT NULL,
  `id_product` int(64) NOT NULL,
  `id_topping` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chain`
--

CREATE TABLE `chain` (
  `id` int(32) NOT NULL,
  `id_user` int(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `photo` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `chain`
--

INSERT INTO `chain` (`id`, `id_user`, `name`, `description`, `location`, `photo`) VALUES
(2, 2, 'MCdonnalds', 'comida rapida', 'USA', NULL),
(3, 2, 'Domino\'s', 'pizza', 'USA', NULL),
(4, 1, 'Tacos pepe', 'tacos', 'MEX', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `makeorder`
--

CREATE TABLE `makeorder` (
  `id` int(128) NOT NULL,
  `id_order` int(128) NOT NULL,
  `id_product` int(128) NOT NULL,
  `id_status` int(1) NOT NULL,
  `type` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id` int(32) NOT NULL,
  `id_user` int(32) NOT NULL,
  `id_truck` int(32) NOT NULL,
  `tax` float NOT NULL DEFAULT 0,
  `total` float NOT NULL,
  `done` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(64) NOT NULL,
  `id_truck` int(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(16) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL,
  `description` varchar(250) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `points` int(8) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `id_truck`, `name`, `code`, `price`, `discount`, `description`, `photo`, `points`) VALUES
(1, 1, 'Hamburguesa', 'hamb1', 10001, 0, 'Hamburguesa clasica', '1.jpeg', 10),
(3, 1, 'Pizza', 'pzz1', 4000, 0, 'Pizza', '3.jpeg', 10),
(4, 1, 'Perro', 'prr1', 7000, 0, 'Perro', '4.jpeg', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'chain', 'Chain admin'),
(3, 'truck', 'Truck admin'),
(4, 'client', 'Client');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(1) NOT NULL,
  `name` varchar(48) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`, `description`) VALUES
(1, 'En espera', 'En espera de preparaci&oacute;n del producto.'),
(2, 'En preparaci&oacute;n', 'El producto se esta preparando'),
(3, 'En entrega', 'El producto espera llegar al cliente'),
(4, 'Entregado', 'Entregado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `topping`
--

CREATE TABLE `topping` (
  `id` int(64) NOT NULL,
  `id_truck` int(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(250) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `topping`
--

INSERT INTO `topping` (`id`, `id_truck`, `name`, `description`, `price`) VALUES
(1, 1, 'Salsa de tomate', 'Salsa de tomate', 200),
(4, 1, 'Salsa de ajo', 'Salsa de ajo', 200),
(5, 1, 'Salsa de pi&ntilde;a', 'Salsa de pi&ntilde;a\r\n', 200),
(6, 1, 'Papas a la francesa', 'Papas a la francesa', 1500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `truck`
--

CREATE TABLE `truck` (
  `id` int(32) NOT NULL,
  `id_chain` int(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `truck`
--

INSERT INTO `truck` (`id`, `id_chain`, `name`, `description`, `location`, `city`, `country`) VALUES
(1, 2, 'Carro central', 'Carro de prueba', 'Salida sur centro comercial Santa Fe', 'Bogot&aacute;', 'Colombia'),
(3, 2, 'Carro 2', 'asdasd', 'asdasd', 'Cali', 'Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(32) NOT NULL,
  `id_role` int(32) NOT NULL,
  `id_truck` int(32) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(220) NOT NULL,
  `cellphone` varchar(64) NOT NULL,
  `date` date DEFAULT NULL,
  `gender` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `id_role`, `id_truck`, `name`, `email`, `password`, `cellphone`, `date`, `gender`) VALUES
(1, 1, NULL, 'admin', 'admin@admin.com', '$2y$10$EbOdCRVZ3VBijufOvv/ptu96yegNRwGDfd.QpiF2qlTrM.7ADU2Qi', '1111', '2010-10-10', 0),
(2, 2, NULL, 'manag', 'manager@manager.com', '$2y$10$EbOdCRVZ3VBijufOvv/ptu96yegNRwGDfd.QpiF2qlTrM.7ADU2Qi', '444', '2020-11-03', 0),
(6, 3, 1, 'empleado', 'empleado@empleado.com', '$2y$10$kzNd9okFJyDArlxltnEG5OSA2UkmP7cKor6D7ppcqlH63.8oxQ3wi', '12333', '2020-11-09', 2),
(7, 3, 3, 'empleado 2', 'empleado2@empleado.com', '$2y$10$d3AQk0fn9RnEYCXq3HBS3eT9J2eCL5YKQ/TtyaXc0bRIeNNHU2Tfu', '441254123', '2020-11-02', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `addtopping`
--
ALTER TABLE `addtopping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addtopping_fk0` (`id_makeorder`),
  ADD KEY `addtopping_fk1` (`id_product`),
  ADD KEY `addtopping_fk2` (`id_topping`);

--
-- Indices de la tabla `chain`
--
ALTER TABLE `chain`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `chain_fk1` (`id_user`);

--
-- Indices de la tabla `makeorder`
--
ALTER TABLE `makeorder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `makeorder_fk0` (`id_order`),
  ADD KEY `makeorder_fk1` (`id_product`),
  ADD KEY `makeorder_fk2` (`id_status`);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_fk0` (`id_user`),
  ADD KEY `order_fk2` (`id_truck`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `photo` (`photo`),
  ADD KEY `product_fk0` (`id_truck`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `topping`
--
ALTER TABLE `topping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `topping_fk0` (`id_truck`);

--
-- Indices de la tabla `truck`
--
ALTER TABLE `truck`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `truck_fk0` (`id_chain`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `user_fk0` (`id_role`),
  ADD KEY `user_fk1` (`id_truck`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `addtopping`
--
ALTER TABLE `addtopping`
  MODIFY `id` int(164) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `chain`
--
ALTER TABLE `chain`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `makeorder`
--
ALTER TABLE `makeorder`
  MODIFY `id` int(128) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(64) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `topping`
--
ALTER TABLE `topping`
  MODIFY `id` int(64) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `truck`
--
ALTER TABLE `truck`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `addtopping`
--
ALTER TABLE `addtopping`
  ADD CONSTRAINT `addtopping_fk0` FOREIGN KEY (`id_makeorder`) REFERENCES `makeorder` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addtopping_fk1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addtopping_fk2` FOREIGN KEY (`id_topping`) REFERENCES `topping` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `chain`
--
ALTER TABLE `chain`
  ADD CONSTRAINT `chain_fk1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `makeorder`
--
ALTER TABLE `makeorder`
  ADD CONSTRAINT `makeorder_fk0` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `makeorder_fk1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `makeorder_fk2` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_fk0` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_fk2` FOREIGN KEY (`id_truck`) REFERENCES `truck` (`id`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_fk0` FOREIGN KEY (`id_truck`) REFERENCES `truck` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `topping`
--
ALTER TABLE `topping`
  ADD CONSTRAINT `topping_fk0` FOREIGN KEY (`id_truck`) REFERENCES `truck` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `truck`
--
ALTER TABLE `truck`
  ADD CONSTRAINT `truck_fk0` FOREIGN KEY (`id_chain`) REFERENCES `chain` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_fk0` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_fk1` FOREIGN KEY (`id_truck`) REFERENCES `truck` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
