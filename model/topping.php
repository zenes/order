<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto topping de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class Topping extends DAO implements Crud
{
    private $id;
    private $id_truck;
    private $name;
    private $description;
    private $price;

    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crear el rol
     * 
     * @param array $data es un array con los datos de la creacion
     *                    del topping
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data)
    {
        $this->id_truck = $data["id_truck"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->price = $data["price"];

        return $this->insert(
            "topping(
            id_truck, 
            name,
            description,
            price
          )", 
            array(
              $this->id_truck,
              $this->name,
              $this->description,
              $this->price
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los toppings
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("topping", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo retorna toda la informacion de todos los toppings por carro
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readTruck($id)
    {
        $this->id_truck = $id;
        return $this->select("topping", "*", array($this->id_truck), "id_truck = ?");
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->price = $data["price"];

        return $this->up(
            "topping", 
            array(
            "name" => $this->name,
            "description" => $this->description,
            "price" => $this->price,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina un topping
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boll
     */    
    public function delete($id)
    {
        $this->id = $id;
        return $this->del("topping", "id", $this->id);
    }
}
?>
