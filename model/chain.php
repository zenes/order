<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto chain de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Chain extends DAO implements Crud
{
    private $id;
    private $id_user;
    private $name;
    private $description;
    private $location;
    private $photo;


    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crea la cadena de restaurantes
     *
     * @param array $data es un arreglo con los datos de la cadena
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return int
     */ 
    public function create($data)
    {
        $this->id_user = $data["id_user"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->location = $data["location"];

        return $this->insert(
            "chain(id_user, name, description, location)", 
            array(
                $this->id_user,
                $this->name,
                $this->description,
                $this->location
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de una o todas las cadenas
     *
     * @param int $id es el id de la cadena
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Chain
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("chain", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }
    /**
     * Este metodo retorna toda la informacion de las ultimas 5 cadenas
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Chain
     */ 
    public function readLatest()
    {
        return $this->select("chain ORDER BY id DESC LIMIT 5", "*");
    }

    /**
     * Este metodo retorna toda la informacion de una cadena por id de manager
     *
     * @param int $id es el id del manager
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Chain
     */ 
    public function readManager($id)
    {
        return $this->select("chain", "*", array($id), "id_user = ?")[0];
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un arreglo con los datos a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->id_user = $data["id_user"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->location = $data["location"];

        return $this->up(
            "chain", 
            array(
            "name" => $this->name,
            "id_user" => $this->id_user,
            "description" => $this->description,
            "location" => $this->location,
            "id" => $this->id
            )
        );
    }

    /**
     * Este metodo se encarga de actualizar el nombre de la foto de una
     * cadena
     *
     * @param array $data es un arreglo con los datos a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function updatePhoto($data)
    {
        $this->photo = $data["photo"];
        $this->id = $data["id"];

        return $this->up(
            "chain", 
            array(
            "photo" => $this->photo,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina una cadena
     * 
     * @param int $id es el id de la cadena a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */    
    public function delete($id)
    {
        $this->id = $id;

        return $this->del("chain", "id", $this->id);
    }
}
?>
