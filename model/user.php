<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto user de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class User extends DAO implements Crud
{
    private $id;
    private $id_role;
    private $id_truck;
    private $name;
    private $email;
    private $password;
    private $cellphone;
    private $date;
    private $gender;

    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crear el rol
     * 
     * @param array $data es un array con los datos de la creacion
     *                    del user
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data)
    {
        $this->id_role = $data["id_role"];
        $this->id_truck = @$data["id_truck"];
        $this->name = $data["name"];
        $this->email = $data["email"];
        $this->password = password_hash($data["password"], PASSWORD_DEFAULT);
        $this->cellphone = $data["cellphone"];
        $this->date = $data["date"];
        $this->gender = $data["gender"];

        return $this->insert(
            "user(
              id_role,
              id_truck,
              name,
              email,
              password,
              cellphone,
              date,
              gender
          )", 
            array(
            $this->id_role,
            @$this->id_truck,
            $this->name,
            $this->email,
            $this->password,
            $this->cellphone,
            $this->date,
            $this->gender
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los users
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("user", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo retorna toda la informacion de todos los clientes
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readClients()
    {
        $this->id_role = 4;
        return $this->select("user", "*", array($this->id_role), "id_role = ?");
    }

    /**
     * Este metodo retorna la informacion de un usuario por email
     * 
     * @param string $email es el email a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readEmail($email)
    {
        $this->email = $email;
        return @$this->select(
            "user INNER JOIN role 
            ON(role.id = user.id_role)", 
            "user.id AS id, user.name AS name, email, cellphone, 
            password, role.name AS role, id_role, id_truck", 
            array($this->email), "email = ?"
        )[0];
    }

    /**
     * Este metodo retorna la informacion de los managers
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readManagers()
    {
        return $this->select("user", "*", array(2), "id_role = 2");
    }

    /**
     * Este metodo retorna la informacion de los empleados de una cadena
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readChain($id)
    {
        return $this->select(
            "user AS u INNER JOIN truck AS t ON(t.id = u.id_truck)", 
            "u.id AS id, u.name AS name, t.name AS truck, cellphone, id_truck, email, id_chain", 
            array($id), "id_chain = ?"
        );
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->id_role = $data["id_role"];
        @$this->id_truck = @$data["id_truck"];
        $this->name = $data["name"];
        $this->email = $data["email"];
        $this->cellphone = $data["cellphone"];
        $this->date = $data["date"];
        $this->gender = $data["gender"];

        return $this->up(
            "user", 
            array(
            "id_role" => $this->id_role,
            "id_truck" => @$this->id_truck,
            "name" => $this->name,
            "email" => $this->email,
            "cellphone" => $this->cellphone,
            "date" => $this->date,
            "gender" => $this->gender,
            "id" => $this->id
            )
        );
    }

    /**
     * Este metodo se encarga de actualizar la contraseña de un usuario
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function updatePassword($data)
    {
        $this->password = $data["password"];
        $this->id = $data["id"];
        $user = $this->read($this->id);

        if (!password_verify($this->password, $user->password)) {
            return false;
        }

        return $this->up(
            "user", 
            array(
            "password" => password_hash($this->id_role, PASSWORD_DEFAULT),
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina un user
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boll
     */    
    public function delete($id)
    {
        $this->id = $id;
        return $this->del("user", "id", $this->id);
    }
}
?>
