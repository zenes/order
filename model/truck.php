<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto truck de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Truck extends DAO implements Crud
{
    private $id;
    private $id_chain;
    private $name;
    private $description;
    private $location;
    private $city;
    private $country;


    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crea la cadena de restaurantes
     *
     * @param array $data es un arreglo con los datos del carro
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return int
     */ 
    public function create($data)
    {
        $this->id_chain = $data["id_chain"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->location = $data["location"];
        $this->city = $data["city"];
        $this->country = $data["country"];

        return $this->insert(
            "truck(id_chain, name, description, location, city, country)", 
            array(
                $this->id_chain,
                $this->name,
                $this->description,
                $this->location,
                $this->city,
                $this->country
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los carros
     *
     * @param int $id es el id del carro
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Truck
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("truck", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo retorna toda la informacion de los carros de una cadena
     *
     * @param int $id es el id de la cadena
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Truck
     */ 
    public function readChain($id)
    {
        $this->id_chain = $id;
        return $this->select("truck", "*", array($this->id_chain), "id_chain = ?");
    }

    /**
     * Este metodo retorna toda la informacion de los ultimos carros por cadena
     *
     * @param int $id es el id de la cadena
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Truck
     */ 
    public function readLatest($id = null)
    {
        return $this->select("truck", "*", array($id), "id_chain = ? ORDER BY id DESC LIMIT 5");
    }

    /**
     * Este metodo se encarga de actualizar los datos de un carro
     *
     * @param array $data es un arreglo con los datos a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->name = $data["name"];
        $this->description = $data["description"];
        $this->location = $data["location"];
        $this->city = $data["city"];
        $this->country = $data["country"];

        return $this->up(
            "truck", 
            array(
            "name" => $this->name,
            "description" => $this->description,
            "location" => $this->location,
            "city" => $this->city,
            "country" => $this->country,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina una cadena
     * 
     * @param int $id es el id de la cadena a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */    
    public function delete($id)
    {
        $this->id = $id;

        return $this->del("truck", "id", $this->id);
    }
}
?>
