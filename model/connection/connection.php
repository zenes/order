<?php

/**
 * PHP Version 7.4.3
 *
 * @category MYSQL_PDO_CONNECION
 * @package  Connection
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
require_once __DIR__."/../../config/config.php";
/**
 * Esta clase es la encargada de representar el objeto de conexion
 * a la base de datos de tipo PDO
 *
 * @category MYSQL_PDO_CONNECION
 * @package  Connection
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
class Connection
{
    /**
     * Instancia de la configuracion para la conexion
     */
    private $_conf;
    /**
     * Instancia de PDO
     */
    public function __construct()
    {
        try {
            // Se traen los datos para la conexion
            $this->_conf = new Config();
            $this->_conf = $this->_conf->getDB();
        } catch (PDOException $e) {
            error_log($e, 3, __DIR__."/error.log");
            return false;
        }
    }
    /**
     * Crear una nueva conexión PDO basada
     * en los datos de conexión
     *
     * @return PDO Objeto PDO
     */
    public function getDB()
    {
        // Se inicializa un nuevo objeto de tipo PDO
        $driver = new PDO(
            $this->_conf['type'].":dbname=".$this->_conf['db'].
            ";host=".$this->_conf['host'].
            ";port:".$this->_conf['port'].
            ";", $this->_conf['user'], $this->_conf['password'],
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
        // Habilitar excepciones
        unset($this->_conf);
        $driver->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $driver;
    }
}
?>
