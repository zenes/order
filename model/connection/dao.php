<?php
/**
 * PHP version 7.4.3
 *
 * @category MYSQL_CONNECTION
 * @package  DAO
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://unividafup.edu.co
 */
require_once __DIR__."/connection.php";
/**
 * Esta clase es la encargada de acceder a la base de datos y ejecutar 
 * las funciones del CRUD a travez de un objeto del tipo PDO
 *
 * @category MYSQL_CONNECTION
 * @package  DAO
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://unividafup.edu.co
 */ 
class DAO
{
    private $_pdo;
    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    protected function __construct()
    {
    }
    /**
     * Esta función es la encargada de realizar la ejecucion
     * de la consulta sql y la captura de errores.
     *
     * @param string  $sql    es un string con la consulta para la base de datos
     * @param array   $params es un array para la base de datos
     * @param boolean $read   es una variable de tipo booleana 
     *                        si es false solo se retorna la
     *                        ejecución de la cunsulta y no
     *                        los datos.
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return int/DAO:Error
     */ 
    private function _sqlExecute($sql, $params = array(1), $read = false)
    {
        try {
            // INICIA CONEXION A BASE DE DATOS
            $this->_pdo = new Connection();
            $this->_pdo = $this->_pdo->getDB();
            $query = $this->_pdo->prepare($sql);
            if ($read) {
                $query->execute($params);
                return $query;
            }
            if ($res = $query->execute($params)) {
                $res_id = $this->_pdo->lastInsertId();
                $res = $res_id ? $res_id : $res;
            }
            return $res;
        } catch (PDOException $e) {
            $result = ini_get("display_errors") ? $e : false;
            return $result;
            return false;
        }
    }

    /**
     * Esta función inserta datos en la base de datos
     *
     * @param string $table  recibe el nombre de la tabla a consultar
     * @param array  $params es un arreglo con los datos como el id que 
     *                       se necesita para la consulta
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return int (id_del ultimo dato insertado en la tabla)
     */ 
    protected function insert($table, $params)
    {
        $sql = "INSERT INTO ".$table." VALUES(";
        for ($i=1; $i < sizeof($params); $i++) {
            $sql.="?,";
        }
        $sql.="?)";
        return $this->_sqlExecute($sql, $params);
    }

    /**
     * Esta funcion selecciona los datos de la base de datos. 
     *
     * @param string $table  recibe el nombre de la tabla a consultar
     * @param string $data   recibe un string con los campos a consultar
     * @param array  $params es un arreglo con los datos como el id que 
     *                       se necesita para la consulta
     * @param string $where  es el condicional para la consulta 
     *                       en caso de ser necesario
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boolean
     */ 
    protected function select($table, $data, $params = null, $where = null)
    {
        $sql = "SELECT ".$data." FROM ".$table;
        $sql .= !empty($where) ? " WHERE ".$where : "";
        if ($dbo = $this->_sqlExecute($sql, $params, true)) {
            return $dbo->fetchAll(PDO::FETCH_OBJ|PDO::FETCH_PROPS_LATE);
        }
        return false;
    }
    /**
     * Esta función actualiza la tabla con informacion nueva
     *
     * @param string $table  recibe el nombre de la tabla a consultar
     * @param array  $params es un arreglo con los datos como el id que 
     *                       se necesita para la consulta
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boolean
     */ 
    protected function up($table, $params)
    {
        $sql = "UPDATE ".$table." SET ";
        $i = 1;
        $tam = sizeof($params);
        $params2 = array();
        foreach ($params as $key => $value) {
            array_push($params2, $value);
            if ($i<$tam) {
                if (!is_null($value) || $value != "") {
                    $sql.=$key." = ? ";
                    if ($i<$tam-1) {
                        $sql.=", ";
                    }
                }
            } else {
                $where = $key;
                $id = $value;
                break;
            }
            $i++;
        }
        $sql.=" WHERE ".$where." = ?";
        return $this->_sqlExecute($sql, $params2);
    }
    /**
     * Esta función elimina un dato de la base de datos
     *
     * @param string $table recibe el nombre de la tabla a consultar
     * @param string $where es el condicional para la consulta 
     *                      en caso de ser necesario
     * @param int    $id    es el id del dato a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boolean
     */ 
    protected function del($table, $where, $id)
    {
        $sql = "DELETE FROM ".$table." WHERE ".$where." = ?"; 
        return $this->_sqlExecute($sql, array($id));
    }
}
?>
