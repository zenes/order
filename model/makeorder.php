<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto makeorder de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class MakeOrder extends DAO implements Crud
{
    private $id;
    private $id_order;
    private $id_product;
    private $id_status;
    private $type; //type 2 para levar, 1 domicilio, type 0 en el sitio

    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crear la orden
     * 
     * @param array $data es un array con los datos de la creacion
     *                    de la orden
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data)
    {
        $this->id_order = $data["id_order"];
        $this->id_product = $data["id_product"];
        $this->id_status = $data["id_status"];
        $this->type = $data["type"]; //type 1 para llevar, 2 domicilio, 0 en el sitio

        return $this->insert(
            "makeorder(
              id_order, 
              id_product,
              id_status,
              type
          )", 
            array(
            $this->id_order,
            $this->id_product,
            $this->id_status,
            $this->type
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los makeorders
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("makeorder", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->id_order = $data["id_order"];
        $this->id_product = $data["id_product"];
        $this->id_status = $data["id_status"];
        $this->type = $data["type"]; //type 1 para llevar, 2 domicilio, 0 en el sitio

        return $this->up(
            "makeorder", 
            array(
            "id_order" => $this->id_order,
            "id_product" => $this->id_product,
            "id_status" => $this->id_status,
            "type" => $this->type,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina un makeorder
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boll
     */    
    public function delete($id)
    {
        $this->id = $id;
        return $this->delete("makeorder", "id", $this->id);
    }
}
?>
