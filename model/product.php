<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto product de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class Product extends DAO implements Crud
{
    private $id;
    private $id_truck;
    private $name;
    private $code;
    private $price;
    private $discount;
    private $photo;
    private $points;

    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crear el rol
     * 
     * @param array $data es un array con los datos de la creacion
     *                    del product
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data)
    {
        $this->id_truck = $data["id_truck"];
        $this->name = $data["name"];
        $this->code = $data["code"];
        $this->price = $data["price"];
        $this->discount = $data["discount"];
        $this->description = $data["description"];
        $this->points = $data["points"];

        return $this->insert(
            "product(
              id_truck,
              name,
              code,
              price,
              discount,
              description,
              points
          )", 
            array(
            $this->id_truck ,
            $this->name ,
            $this->code ,
            $this->price ,
            $this->discount ,
            $this->description ,
            $this->points 
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los products
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("product", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo retorna toda la informacion de los productos por carro
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readTruck($id = null)
    {
        $this->id_truck = $id;
        return $this->select("product", "*", array($this->id_truck), "id_truck = ?");
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->name = $data["name"];
        $this->code = $data["code"];
        $this->price = $data["price"];
        $this->discount = $data["discount"];
        $this->points = $data["points"];

        return $this->up(
            "product", 
            array(
            "name" => $this->name,
            "code" => $this->code,
            "price" => $this->price,
            "discount" => $this->discount,
            "points" => $this->points,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo se encarga de actualizar el nombre de la foto de un
     * producto
     *
     * @param array $data es un arreglo con los datos a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function updatePhoto($data)
    {
        $this->photo = $data["photo"];
        $this->id = $data["id"];

        return $this->up(
            "product", 
            array(
            "photo" => $this->photo,
            "id" => $this->id
            )
        );
    }
    /**
     * Este metodo elimina un product
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boll
     */    
    public function delete($id)
    {
        $this->id = $id;
        return $this->del("product", "id", $this->id);
    }
}
?>
