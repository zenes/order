<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar una tabla de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */ 
interface Crud
{
    /**
     * Este metodo se encarga de insertar un objeto a la base de datos
     *
     * @param array $data es un arreglo con datos para la creacion del
     *                    objeto
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data);
    /**
     * Este metodo se encarga de retornar la informacion de la base de datos
     *
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function read($id = null);
    /**
     * Este metodo se encarga de actualizar un objeto a la base de datos
     *
     * @param array $data es un arreglo con datos para la actualizacion del
     *                    objeto
     *                    
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function update($data);
    /**
     * Este metodo se encarga de eliminar un objeto de la base de datos
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function delete($id);
}
