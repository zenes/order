<?php
/**
 * PHP version 7.4.3
 *
 * @category Entidades_BD
 * @package  Model
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */

require_once __DIR__."/crud.php";
require_once __DIR__."/connection/dao.php";

/**
 * Esta clase es la encargada de representar el objeto order de la base de datos
 *
 * @category Class
 * @package  Usuario
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
class Order extends DAO implements Crud
{
    private $id;
    private $id_truck;
    private $id_user;
    private $tax;
    private $total;

    /**
     * Este es el metodo constructor de la clase
     * 
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Este metodo crear la orden
     * 
     * @param array $data es un array con los datos de la creacion
     *                    de la orden
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return Void
     */ 
    public function create($data)
    {
        $this->id_user = $data["id_user"];
        $this->id_truck = $data["id_truck"];
        $this->tax = $data["tax"];
        $this->total = $data["total"];

        return $this->insert(
            "order(
              id_user, 
              id_truck, 
              tax,
              total,
          )", 
            array(
              $this->id_user,
              $this->id_truck,
              $this->tax,
              $this->total
            )
        );
    }

    /**
     * Este metodo retorna toda la informacion de uno o todos los orders
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function read($id = null)
    {
        $this->id = $id;

        $return = function ($params = null, $where = null) {
            return $this->select("order", "*", $params, $where);
        };
        if (!$this->id) {
            return $return();
        }
        return $return(array($this->id), "id = ?")[0];
    }

    /**
     * Este metodo retorna toda la informacion de las ordenes por camion
     * 
     * @param int $id es el id a leer
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return array/Role
     */ 
    public function readTruck($id = null)
    {
        $this->id_truck = $id;
        return $this->select("order", "*", array($this->id_truck), "id_truck = ?");
    }

    /**
     * Este metodo se encarga de actualizar los datos de una cadena
     *
     * @param array $data es un array con la informacion a actualizar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return bool
     */ 
    public function update($data)
    {
        $this->id = $data["id"];
        $this->tax = $data["tax"];
        $this->total = $data["total"];

        return $this->up(
            "order", 
            array(
            "tax" => $this->tax,
            "total" => $this->total,
            "id" => $this->id
            )
        );
    }
 
    /**
     * Este metodo elimina un order
     *
     * @param int $id es el id a eliminar
     *
     * @author José Camilo Jiménez <jmilo@protonmail.com>
     * @return boll
     */    
    public function delete($id)
    {
        $this->id = $id;
        return $this->delete("order", "id", $this->id);
    }
}
?>
