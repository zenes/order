<?php
/**
 * PHP version 7.4.3
 *
 * @category View
 * @package  View
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/view/layout/layout_index.php";
require_once __DIR__."/config/config.php";
$config = new Config();
$layout = new LayoutIndex();
$extraCss = "
";
$layout->metaHead($extraCss);
?>
    <section id="wrapper">
        <?php if(@$_GET['error'] === '1') :?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Error!</strong> Los datos ingresados no coinciden, intente de nuevo.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php elseif(@$_GET['success'] === '1') :?>

        <?php endif;?>
       <div class="login-register" style="background-image:url(<?php echo $config->getUri("assets/images/background/background.jpg")?>);">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" method="POST" id="loginform" action="<?php echo $config->getUri("controller/user_con/login")?>">
                        <div class="text-center">
                            <img style="width:100%" src="<?php echo $config->getUri("assets/images/logo-big.png")?>" alt="">
                        </div>
                        <hr>
                        <h3 class="text-center m-b-20">Iniciar sesión</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="email" name="email" required="required" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password" required="required" placeholder="contraseña"> </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                  <a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Recuperar contraseña</a> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-success btn-rounded" type="submit">Ingresar</button>
                            </div>
                            <div class="col-xs-12 p-b-20">
                                <a href="chains" class="btn btn-block btn-lg btn-info btn-rounded" type="button">Zona clientes</a>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal" id="recoverform" action="index.html">
                        <div class="form-group "> <div class="col-xs-12"> <h3>Recuperar contraseña</h3>
                                <p class="text-muted">Ingresa tu email y documento</p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="email" required="" placeholder="Email">
                                <input class="form-control" type="text" required="" placeholder="Documento"> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-success btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Enviar</button>
                            </div>
                        </div>
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" onclick="$('#recoverform').fadeOut();$('#loginform').fadeIn();" type="button">Atras</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php
$layout->footer();
?>
<script>
var cookie = document.cookie.match("(^|[^;]+)\\s*cookies_syllabus\\s*=\\s*([^;]+)");
cookie ? cookie.pop() : "";
if(cookie == null){
  verTerminosCookies();
}
function verTerminosCookies(){
  $("#tituloModal").empty();
  $("#cuerpoModal").empty();
  $("#tituloModal").append("Acerca de nuestros cookies");
  $("#cuerpoModal").append("Nuestro sitio solo utilíza cookies para el manejo de la sesion del usuario y las notificaciones. Nunca serán usados con fines comerciales. \
Al cerrar este recuadro usted acepta las condiciones planteadas.\
<br><br>\
");
  $("#miModal").modal({backdrop: 'static', keyboard: false});
}
</script>
</html>
