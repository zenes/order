<?php
/**
 * PHP version 7.4.3
 *
 * @category Vista
 * @package  View
 * @author   José camilo Jiménez <jmilo@protonmaill.con>
 * @license  MIT
 * @link     https://pbear.xyz
 */
require_once __DIR__."/view/layout/layout_client.php";
$layout = new Layout_Client();
$layout->metaHead();
$layout->navBar();
$layout->sideBar();
?>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Inicio</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Info box -->
    <!-- ============================================================== -->
    <div class="card-group">
<div class="row">
                    <div class="col-12">
                        <h4 class="d-inline">Restaurantes</h4>
                        <p class="text-muted m-t-0">Lista de restaurantes</p>
                        <!-- Row -->
                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-3 col-md-6">
                                <!-- Card -->
                                <div class="card">
                                    <img class="card-img-top img-responsive" src="assets/uploads/chains/2.jpeg" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-title">Mcdonalds</h4>
                                        <p class="card-text">Mcdonalds</p>
                                        <a href="truck?id=2" class="btn btn-primary">IR</a>
                                    </div>
                                </div>
                                <!-- Card -->
                            </div>
                            <!-- column -->
                            <!-- column -->
                            <div class="col-lg-3 col-md-6">
                                <!-- Card -->
                                <div class="card">
                                    <img class="card-img-top img-responsive" src="assets/uploads/chains/3.jpeg" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-title">Dominos</h4>
                                        <p class="card-text">Dominos</p>
                                        <a href="truck?id=3" class="btn btn-primary">IR</a>
                                    </div>
                                </div>
                                <!-- Card -->
                            </div>
                            <!-- column -->
                            <!-- column -->
                            <!-- column -->
                            <!-- column -->
                            <div class="col-lg-3 col-md-6 img-responsive">
                                <!-- Card -->
                                <div class="card">
                                    <img class="card-img-top img-responsive" src="assets/uploads/chains/10.jpeg" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-title">Tacos Pacco</h4>
                                        <p class="card-text">Tacos pacco</p>
                                        <a href="truck?id=10" class="btn btn-primary">IR</a>
                                    </div>
                                </div>
                                <!-- Card -->
                            </div>
                            <!-- column -->
                        </div>
                        <!-- Row -->
                    </div>
                </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Info box -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Over Visitor, Our income , slaes different and  sales prediction -->
    <!-- ============================================================== -->
</div>
<?php
$layout->footer();
?>
