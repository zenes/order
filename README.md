THE ORDER
--------
# Aplicación

La aplicación se desarrollo siguiendo una patron de diseño MVC usando php sin ningún tipo de framework, 
quedando dividida en 3 capas; la vista (view), el controlador (controller) y el modelo (model).


La razón detras del desarrollo usando un patron MVC se debe a que aporta muchas ventajas a la hora de darle mantenimiento a la aplicación
debido a que todos los componentes quedan debidamente separados y se puede distinguir el trabajo de cada una de sus partes.

por cuestiones de tiempo, el aplicativo quedo en un 85% completado, por el momento permite realizar las siguientes tareas:

- Registrar una cadena de restaurantes (los dueños de los carros), a través de la cuenta de administrador:
  
  -correo: admin@admin.com, contraseña: 1234

- Registrar los carros o puntos de venta de la cadena de restaurantes (managers):
  
  -correo: manager@manager.com, contraseña: 1234

- Registrar los productos y añadidos a través de la cuenta del vendedor:
  
  -correo: empleado@empleado.com, contraseña: 1234

También permite la modificación, creación y eliminación de todo lo nombrado anteriormente.

Queda faltando la interfaz de cliente quien es el que hace los pedidos al carro de ventas.

Para el funcionamiento es necesario una versión de PHP superior a la 7.0, PDO y tener habilitado el modulo rewrite de apache.

El sql de la base de datos queda guardado en el directorio config al igual que el diagrama ER

Si se quiere cambiar el nombre de la aplicación o se quiere editar los datos de acceso a la base de datos, hay que modificar el archivo
config.php que se encuentra en la carpeta config, en el se encuentran los parametros de configuracion y las rutas, ademas de que en caso
de querer modificar las rutas es necesario modificar el archivo .htaccess que se encuentra en el directorio raiz del proyecto.


Lamento no haber podido terminar la aplicación tal y como se pide en el documento, tal vez es por cuestiones de tiempo, de igual manera
todo el código queda comentado y sigue la siguiente estructura:


| Base de datos | => |DAO| => | Modelos | => | Controladores | => | Layout | => | View |

Espero cualquier sugerencia. muchas gracias por la oportunidad y por el reto, hace bastante no encontraba un reto así.

-------------------------------------------------------------------------------------------------

# Historias de usuario


## Admin

### Historia 1
- Como administrador, quiero iniciar sesión para ingresar al panel del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 2
- Como administrador, quiero ver los usuarios del sitio
- Complejidad: 2
- Estado: finalizado.

### Historia 3
- Como administrador, quiero ver las cadenas de restaurantes
- Complejidad: 2
- Estado: finalizado.

### Historia 4
- Como administrador, quiero ver los estados para los pedidos
- Complejidad: 2
- Estado: finalizado.

### Historia 5
- Como administrador, quiero eliminar un usuario del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 6
- Como administrador, quiero eliminar una cadenas de restaurantes
- Complejidad: 2
- Estado: finalizado.

### Historia 7
- Como administrador, quiero eliminar un estado para los pedidos
- Complejidad: 3
- Estado: finalizado.

### Historia 8
- Como administrador, quiero editar un usuarios del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 9
- Como administrador, quiero editar una cadenas de restaurantes
- Complejidad: 3
- Estado: finalizado.

### Historia 10
- Como administrador, quiero editar un estado para los pedidos
- Complejidad: 3
- Estado: finalizado.

## Manager (Dueño de restaurantes)

### Historia 1
- Como manager, quiero iniciar sesión para ingresar al panel del sitio
- Complejidad: 1
- Estado: finalizado.

### Historia 2
- Como manager, quiero ver los empleados del sitio
- Complejidad: 2
- Estado: finalizado.

### Historia 3
- Como manager, quiero ver las carros del restaurante
- Complejidad: 2
- Estado: finalizado.

### Historia 5
- Como manager, quiero eliminar un empleado del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 6
- Como manager, quiero eliminar un carro de restaurantes
- Complejidad: 2
- Estado: finalizado.

### Historia 7
- Como manager, quiero editar un empleado del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 8
- Como manager, quiero editar una carro de restaurantes
- Complejidad: 3
- Estado: finalizado.

## Carro (Carro de distribución de comida)

### Historia 1
- Como vendedor, quiero iniciar sesión para ingresar al panel del sitio
- Complejidad: 1
- Estado: finalizado.

### Historia 2
- Como vendedor, quiero ver los productos de mi lugar
- Complejidad: 2
- Estado: finalizado.

### Historia 3
- Como vendedor, quiero ver la lista de añadidos del sitio
- Complejidad: 2
- Estado: finalizado.

### Historia 4
- Como vendedor, quiero ver la lista de ordenes para hacer los pagos
- Complejidad: 2
- Estado: finalizado.

### Historia 5
- Como vendedor, quiero eliminar un producto del sitio
- Complejidad: 3
- Estado: finalizado.

### Historia 6
- Como vendedor, quiero eliminar un añadido de restaurantes
- Complejidad: 2
- Estado: finalizado.

### Historia 8
- Como vendedor, quiero editar un producto de restaurantes
- Complejidad: 3
- Estado: finalizado.

### Historia 8
- Como vendedor, quiero editar un añadido de restaurantes
- Complejidad: 3
- Estado: finalizado.

